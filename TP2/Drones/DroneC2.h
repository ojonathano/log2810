#pragma once

#include "Drones.h"
#include <iostream>

using namespace std;

class DroneC2 : public Drone {
public:
	DroneC2();
	DroneC2(int& pt);

	virtual ~DroneC2();

	virtual int getPoidsTransporter();
	virtual void setPoidsTransporter(const int& pt);
	virtual int getPoidsMax();
	virtual bool estAssigner();
	virtual void setEstAssigner(const bool& a);

private:
	int pTransporter_;
	int pMax_ = 5;
	bool assigner_; 
};
