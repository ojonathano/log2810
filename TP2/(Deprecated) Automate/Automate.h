#ifndef H_AUTOMATE
#define H_AUTOMATE
#include"../Nodes/Node.h"
#include"../Requetes/Requete.h"
#include <iostream>

//classe automate
class Automate
{
public:
	//constructeurs
	Automate(vector<string> quartiers);
	//destructeur
	~Automate();
	//fonctions tests
	bool isRoot(const Node& node) const;
	bool validFormat(const string& adresse) const;
	bool isFils(const Node& papa, const char& char_fils) const;
	//fonctions de construction
	void addAddr(const string& adresse);//rajoute une adresse
	void rajouter(Node* node, Node*& last);//cr�er le node � rajouter et le rentre dans les enfants de l'autre
	//fonction de parcours
	bool evaluer(string addresse);//parcours les roots et fait des choses diff�rentes si en construction ou pas


private:
	vector<Node*> vocab_;
	vector<Node*> roots_;
	bool enConstr_;
	vector<string> lectureDir_;//ce qui est construit
};

#endif // !H_AUTOMATE
