#include"Automate.h"
#include<algorithm>
#include<functional>
#define ADDRLENGTH 6

using namespace placeholders;

//foncteur utile dans le parcours de l'automate
class compareChars {
public:
	compareChars(const Node& a) :eqt_(equal_to<char>()) {
		a_ = a.obtenirChar();
	}
	double operator()(const Node* b) {
		return eqt_(a_, b->obtenirChar());
	}
private:
	equal_to<char> eqt_;
	char a_;	//
};

Automate::Automate(vector<string> quartiers) : enConstr_{ true }, lectureDir_{quartiers} {
	std::cout << "construction d'un automate, il y a " <<quartiers.size()<<" quartiers a reconnaitre"<< std::endl;
	for each (string quartier in quartiers)
	{
		evaluer(quartier);
	}
	enConstr_ = false;
}



Automate::~Automate() {
	//par composition alors
	roots_.clear();
	vocab_.clear();
}

//permet de savoir si le node pass� en param�tre est d�j� dans les roots du graphe
bool Automate::isRoot(const Node& node) const{
	compareChars charN(node);
	auto it = find_if(roots_.begin(), roots_.end(), charN);//chercher si le node est dans les roots
	if (it == roots_.end()) {//si le node n'est pas dans le vocabulaire
		return false;
	}
	return true;
}

bool Automate::validFormat(const string& adresse) const {
	if (adresse.length() > ADDRLENGTH) {//pas une longueur valide
		return false;
	}
	for (size_t i = 0; i < ADDRLENGTH; i++)
	{
		if (i % 2 == 0) {//pour tout caract�re pair de l'addresse
			if ((adresse[i] < 64 && adresse[i]> 91) || (adresse[i] < 97 && adresse[i]> 122)) {//=>si ce n'est pas ni une majuscule ou minuscule
				return false;//pas un format valide
			}
		}
		if (i % 2 == 1) {//pour tout caract�re impair de l'addresse
			if (adresse[i] < 47 && adresse[i]> 58) {//si ce n'est pas un chiffre
				return false;//pas un format valide
			}
		}
	}
	return true;
}


bool Automate::isFils(const Node& papa, const char& char_fils)const {
	for each (Node* var in papa.obtenirFils()){
		if (to_string(char_fils) == to_string(var->obtenirChar())) {
			return true;
		}
	}
	return false;
}

//ajoute une adresse dans les symboles terminaux de l'automate
void Automate::addAddr(const string& adresse) {
	std::cout << " Etape de construction de l'automate... " << std::endl;
	Node* last = nullptr;//garde en m�moire le node pr�c�dent
	for (size_t i = 0; i < 6; i++)//on regarde l'addresse au complet
	{
		Node* node = new Node(adresse[i]);
		rajouter(node, last);
	}
	std::cout << " pass." << std::endl << std::endl;
}

//permet de faire le lien entre les diff�rents nodes de l'automate
void Automate::rajouter(Node* node, Node*& last) {
	if (enConstr_) {//si on est en construction, sinon pas d'affaire � rajouter des choses dans le graphe
					//========== construction du root =====
		if (roots_.size() == 0) {//si il n'y a pas de racines ==> ce premier caract�re est une racine
			std::cout << " - ajout du caractere:" << node->obtenirChar() << " - new - ROOT INITIAL" << std::endl;
			roots_.push_back(node);//on rajoute 
			vocab_.push_back(node);
			last = node;//adresse du root
		}
		//=====================================
		else { //==== ajout � la structure ====
			std::cout << " - ajout du caractere " << node->obtenirChar() << " - ";
			compareChars charN(*node);
			bool isNew = true;
			auto it = find_if(vocab_.begin(), vocab_.end(), charN);//chercher si le node est dans le vocab
			if (it == vocab_.end()) {//si le node n'est pas dans le vocabulaire
				vocab_.push_back(node);//l'ajouter
				std::cout << "new";
			}
			else {//s'il est dedans
				delete node;// on delete le node superflus
				node = *it._Ptr;//on l'a trouv�
				isNew = false;
				std::cout << "old";
			}
			if (last == nullptr) {//si le last n'est pas initialis�, on est en train de construire un nouveau root
				if (!isRoot(*node)) {//s'il n'est pas d�j� dans les roots
					std::cout << " - NEW ROOT ";
					roots_.push_back(node);//on le rajoute
					//pas � le rajouter aux enfants du last
				}
			}
			else {
				if (!isFils(*last,node->obtenirChar()))//s'il n'est pas encore dans ses fils (donc nouveau)
					last->ajouterEnfant(node);//dire au dernier que celui-ci est son enfant
			}
			std::cout << std::endl;
			last = node;
		}//====================================
	}
}


//parcours les roots et fait des choses diff�rentes si en construction ou pas
bool Automate::evaluer(string adresse) {
	//===================== V�RIFICATION DU FORMAT ====
	if (!validFormat(adresse)) { //v�rification du format
		std::cout << "adresse de format non valide d�tect�e: " << adresse << std::endl;
		return false;
	}
	//=================================================

	//===================== CONSTRUCTION DU GRAPHE ====
	if (enConstr_) {//si on est en train de construire le graphe
		addAddr(adresse);//rajouter cette adresse
		return true;
	}
	//==================================================

	//======================= V�RIFICATION GLOBALE =====
	else {
		std::cout << "evaluation du quartier " << adresse << "... " << std::endl;
		bool pass = false;
		int compteur = 0;//initialisation d'un compteur
		Node* step = nullptr;//garde en m�moire
		//=================== v�rif du Root ======================================
		std::cout<<" - verification de ["<< adresse[compteur] << "]... ";
		for each (Node* var in roots_)//on v�rifie pour les roots
		{
			if (to_string(adresse[compteur]) == to_string(var->obtenirChar())) {
				std::cout <<  "root trouve" << std::endl;
				step = var;
				pass = true;
			}
		}

		if (!pass) {
			std::cout << "la lettre [" << adresse[compteur] << "] n'est pas dans les roots_" << std::endl;
			return false;
		}
		//========================================================================
		compteur++;//incr�mentation
		pass = false;//reset
		//==================== v�rif des fils ====================================
		for (int i = 1; i < ADDRLENGTH; i++)//les 5 prochain caract�res
		{
			std::cout << " - verification de [" << adresse[compteur] << "] en "<<compteur+1<<"e place" << std::endl;
			for each (Node* var in step->obtenirFils())
			{
				if (to_string(adresse[compteur]) == to_string(var->obtenirChar())) {
					step = var;
					pass = true;
				}
			}
			if (!pass) {
				std::cout << "la lettre [" << adresse[compteur] << "] n'est pas dans les fils de [" << step->obtenirChar() << "]" << std::endl;
				return false;
			}
			compteur++;
			pass = false;//reset
		}
		//========================================================================
		std::cout << " pass." << std::endl;
		return true;
	}
	//==================================================
}



