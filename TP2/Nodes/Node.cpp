#include "Node.h"

Node::Node(){}

Node::Node(Node* parent, const string& tN, const std::vector<Node*>& fils) :parent_(parent), ceNode_(tN), fils_(fils) {}

/*Node::Node(Node const& aCopier){
	parent_ = aCopier.obtenirParent();
	ceNode_ = aCopier.obtenirChar();
	fils_ = aCopier.obtenirFils();
}*/

Node::Node(Node* parent, string chaine) {
	this->ceNode_ = chaine;
	parent_ = parent;
}
Node::~Node() {
	//par aggrégation, donc pas de delete
}
Node* Node::obtenirParent() const{
	return parent_;
}

string Node::obtenirChar() const {
	return ceNode_;
}

vector<Node*>& Node::obtenirFils() {
	return fils_;
}