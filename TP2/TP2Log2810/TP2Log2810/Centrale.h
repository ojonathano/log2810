#pragma once

#include "Quartier.h"
#include <vector>
#include "Requete.h"
#include <fstream>
#include <iostream>
#include "FileDeRequete.h"
#include "Drones.h"
#include "DroneC1.h"
#include "DroneC2.h"
#include <map>
#include <algorithm>
#include <stdio.h>      
#include <stdlib.h>   
#include <time.h>
#include <iomanip>
#include <algorithm>
#include "Automate.h"

using namespace std; 

class Centrale {
public: 
	
	Centrale(); 
	Centrale(vector<string> adresse); 

	void creerQuartiers(vector<string> qu);

	void equilibrerFlotte();
	void traiterLesRequetes(FileDeRequete& requetes, ifstream& fichierR, Automate& automate);

	//Seulement a la premier execution
	void repartirFlotte();

	void imprimerStatistiques();
	void imprimerRepartitionFlotte(); 

	int getNbrRequetesTraites();
	int getNbrRequetesInvalides();

private:

	void creerFlote();

	vector<Quartier*> lesQuartiers; 
	vector<Drone*> flotte; 

	int nbrRequetesTraites; 
	int nbrRequetesInvalides; 

	void setQuartier(vector<Quartier*> qu);
	void incCentrale();
	void retirerDrone(Drone* drone, Quartier& qu);
	void deplacerColis(Requete& requete);
	Drone* assignerColis(int& pt, string& adresseDep);
};

