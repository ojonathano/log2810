#include "Requete.h"
#pragma once

/**
* Constructeur.
*/
Requete::Requete() {
	codePost1_ = "unknown";
	codePost2_ = "unknown";
	poids_ = 0; 
	estValide_ = true; 
}

/**
* Constructeur par parametre.
* @param cp1: L'adresse de depart. 
* @param cp2: L'adresse d'arrive.
* @param poids: Le poids transporter. 
*/
Requete::Requete(string& cp1, string& cp2, int& poids) {
	codePost1_ = cp1;
	codePost2_ = cp2; 
	poids_ = poids; 
	estValide_ = true;
}

/**
* Destructeur. 
*/
Requete::~Requete() {} 

/**
* Retourne l'adresse de depart. 
*/
string Requete::getCodePost1() {
	return codePost1_; 
}

/**
* L'adresse d'arrivee.
*/
string Requete::getCodePost2() {
	return codePost2_; 
}

/**
* Retourne le poids transporte.
*/
int Requete::getPoids() {
	return poids_; 
}

/**
* Retourne si la requete est valide.
*/
bool Requete::estValide() {
	return estValide_;
}

/**
* Permet de setter si la requete est valide. 
*/
void Requete::setValidite(const bool& v) {
	estValide_ = v; 
}
