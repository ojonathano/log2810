#pragma once

#include "DroneC1.h"
#include "DroneC2.h"
#include "Drones.h"
#include "FileDeRequete.h"
#include "Requete.h"
#include <iostream>
#include <fstream>
#include "Centrale.h"
#include "Quartier.h"
#include "Automate.h"

void optionB(bool& tF, string& rep, FileDeRequete& file, Centrale& centrale, Automate& automate);
void optionC(bool& tF, char& rep, string& repo, Centrale& centrale);

int main() {
	
	FileDeRequete* file = FileDeRequete::getInstance();
	cout << "Creez l'automate initial du programme." << endl;
	Automate* automate = Automate::getInstance();
	Centrale centrale(automate->getListeSansDoublons());
	centrale.repartirFlotte();

	bool operationTerminer = true;
	while (operationTerminer) {

		char reponse;
		cout << "==================== Menu ===================" << endl;
		cout << "(a) Reconstruire l'automate selon un autre fichier" << endl;
		cout << "(b) Traiter les requetes" << endl;
		cout << "(c) Afficher les statistiques" << endl;
		cout << "(d) Quitter" << endl;
		cout << "Option: " << endl;
		cin >> reponse;
		string repo = "";
		string repo3 = "";
		char repo2;
		bool a = true;

		switch (reponse) {
		case 'a':
			system("cls");
			automate->reinitialiserArbre();
			a = true;
			reponse = 'b';
			break;
		case 'b':
			system("cls");
			optionB(a, repo, *file, centrale, *automate);
			a = true;
			break;
		case 'c':
			optionC(a, repo2, repo3, centrale);
			a = true;
			break;
		case 'd':
			system("cls");
			cout << "Fermeture du programme" << endl;
			operationTerminer = false;
		}
	}
	return 0;
}

void optionB(bool& tF, string& rep, FileDeRequete& file, Centrale& centrale, Automate& automate) {
	while (tF) {
		string fichier;
		cout << "Entrer le nom du fichier requetes a entrer avec l'extension (i.e exemple.txt): " << endl;
		cin >> fichier;
		ifstream fichierRequetes(fichier);
		centrale.traiterLesRequetes(file, fichierRequetes, automate);
		centrale.equilibrerFlotte();
		cout << "Retourner menu principale? Oui / Non: " << endl;
		cin >> rep;
		if (rep == "oui") {
			system("cls");
			tF = false;
		}
	}
}

void optionC(bool& tF, char& rep, string& repo, Centrale& centrale) {
	while (tF) {
		system("cls");
		cout << "(a) Afficher statistiques" << endl;
		cout << "(b) Afficher repartition flotte" << endl;
		cout << "Option: " << endl;
		cin >> rep;

		switch (rep) {
		case 'a':
			system("cls");
			centrale.imprimerStatistiques();
			cout << "Retourner au menu des statistique? Oui / Non: " << endl;
			cin >> repo;
			if (repo == "Oui" || repo == "oui" || repo == "oUi" || repo == "ouI" || repo == "OuI" || repo == "oUI" || repo == "OUi" || repo == "OUI")
				break;

		case 'b':
			system("cls");
			centrale.imprimerRepartitionFlotte();
			cout << "Retourner au menu des statistique? Oui / Non: " << endl;
			cin >> repo;
			if (repo == "Oui")
				break;
		}

		cout << "Retourner menu principale? Oui / Non: " << endl;
		cin >> repo;
		if (repo == "Oui" || repo == "oui" || repo == "oUi" || repo == "ouI" || repo == "OuI" || repo == "oUI" || repo == "OUi" || repo == "OUI") {
			system("cls");
			tF = false;
		}
	}
}