#pragma once

#include "Drones.h"
#include <iostream>

using namespace std;

class DroneC1: public Drone {
public:
	DroneC1();
	DroneC1(const int& pt); 

	virtual ~DroneC1();

	virtual int getPoidsTransporter(); 
	virtual void setPoidsTransporter(const int& pt); 
	virtual int getPoidsMax(); 
	virtual bool estAssigner();
	virtual void setEstAssigner(const bool& a);

private: 
	int pTransporter_; 
	int pMax_ = 1;
	bool assigner_; 
};