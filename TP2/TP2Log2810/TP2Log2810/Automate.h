#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <memory>
#include <queue>
#include "Node.h"
#include "Requete.h"

using namespace std;

class Automate
{
public:
	~Automate();
	//pour instancier l'automate
	static Automate* getInstance();
	vector<string> getListe();

	//retourne une liste contenant les adresses sans doublons qui sera utilis�e pour la la cr�ation des quartiers
	vector<string>& getListeSansDoublons();
	Node* getRacine() const;

	//M�thode permettant de valider une requete
	//Prend ue requete en parametre et retourne un bool
	bool validerRequete(Requete& r);
	//valide une string
	void reinitialiserArbre();

private:
	//il nous a �t� reproch� lors du dernier tp de ne pas demander a l'utilisateur d'entrer au clavier le nom de son fichier
	//Pour cette raison, la fonction ne prend pas en parametre le nom du fichier mais plutot demande a l'utilisateur de lentrer au clavier dans la fonciton
	void creerArbreAdresses();
	//Constructeur priv�, l'automate est un Singleton
	Automate();
	bool lireFichier();
	bool validerAdresse(string adresse);

	//R�initialise l'arbre et lit un nouveau fichier
	vector<string> listeSansDoublons_;
	vector<string> listeAdresse_;
	Node* racine_;
};

