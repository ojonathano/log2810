#pragma once

using namespace std;
class Drone {
public:
	Drone() {};
	virtual ~Drone() {}; 

	virtual int getPoidsTransporter() = 0 {}; 
	virtual void setPoidsTransporter(const int& pt) = 0 {};
	virtual int getPoidsMax() = 0 {}; 
	virtual bool estAssigner() = 0 {}; 
	virtual void setEstAssigner(const bool& a) = 0 {}; 
};