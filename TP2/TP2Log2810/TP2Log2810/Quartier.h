#pragma once

#include "Drones.h"
#include <string>
#include <vector>
using namespace std; 

class Quartier {
public:
	Quartier();
	Quartier(string& adr); 

	string getAdresse(); 
	
	vector<Drone*> getDrones(); 
	void setDrone(vector<Drone*> drone);
	void addDrones(Drone* drones); 

	int getCyclesSD_(); 
	void setCyclesSD_(int& cycleCompteur); 
	void incCyclesSD_(); 

private:
	vector<Drone*> drones_; 
	string adresse;

	//cycles sans drones
	int cyclesSD_;
};
