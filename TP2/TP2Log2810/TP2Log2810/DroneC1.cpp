#pragma once

#include "DroneC1.h"

/**
* Constructeur.
*/
DroneC1::DroneC1() {
	pTransporter_ = 0; 
	assigner_ = false;
}

/**
* Constructeur par parametre.
* @param pt: Le poids transporter.
*/
DroneC1::DroneC1(const int& pt) {
	pTransporter_ = pt; 
}

/**
* Destructeur
*/
DroneC1::~DroneC1() {} 

/**
* Getter. Retourne le poids transporter. 
*/
int DroneC1::getPoidsTransporter() {
	return pTransporter_; 
}

/**
* Setter. Attribue un poids transporter.
* @param pt: Le poids transporter. 
*/
void DroneC1::setPoidsTransporter(const int& pt) {
	pTransporter_ = pt; 
}

/**
* Getter. Retourne le poids maximal que peut transporter un drone de type 1.
*/
int DroneC1::getPoidsMax() {
	return pMax_; 
} 

/**
* Getter. Retourne la valeur de la varible assigner_.
* Permet de savoir si un drone est assigner a un colis.
*/
bool DroneC1::estAssigner() {
	return assigner_; 
}

/**
* Permet d'attribuer une valeur a la variable assigner_.
* Permet donc d'assigner un drone a un colis.
* @param a: La valeur a assigner. 
*/
void DroneC1::setEstAssigner(const bool& a) {
	assigner_ = a; 
}


