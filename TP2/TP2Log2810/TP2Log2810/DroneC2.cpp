#pragma once

#include "DroneC2.h"

/**
* Constructeur.
*/
DroneC2::DroneC2() {
	pTransporter_ = 0;
	assigner_ = false;
}

/**
* Constructeur par parametre.
* @param pt: Le poids transporter. 
*/
DroneC2::DroneC2(int& pt) {
	pTransporter_ = pt;
}

/**
* Destructeur.s
*/
DroneC2::~DroneC2() {}

/**
* Getter. Retourne le poids transporter.
*/
int DroneC2::getPoidsTransporter() {
	return pTransporter_;
}

/**
* Setter. Attribue un poids transporter.
* @param pt: Le poids transporter.
*/
void DroneC2::setPoidsTransporter(const int& pt) {
	pTransporter_ = pt;
}

/**
* Getter. Retourne le poids maximal que peut transporter un drone de type 1.
*/
int DroneC2::getPoidsMax() {
	return pMax_;
} 

/**
* Getter. Retourne la valeur de la varible assigner_.
* Permet de savoir si un drone est assigner a un colis.
*/
bool DroneC2::estAssigner() {
	return assigner_;
}

/**
* Permet d'attribuer une valeur a la variable assigner_.
* Permet donc d'assigner un drone a un colis.
* @param a: La valeur a assigner.
*/
void DroneC2::setEstAssigner(const bool& a) {
	assigner_ = a;
}
