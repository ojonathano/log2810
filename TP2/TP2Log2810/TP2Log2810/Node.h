#ifndef H_NODE
#define H_NODE

/********************************
* name: IntelliNode.cpp
* date: 15 nov 2017
* desc: Fichier implémentant les Nodes utilisés dans l'automate
*********************************/

#include <vector>
using namespace std;

class Node
{
public:
	//constructeurs
	Node();
	Node(Node* parent, const string& tN, const std::vector<Node*>& fils);
	//Node(Node const& aCopier);
	Node(Node* parent, string chaine);
	//destructeurs
	~Node();
	//getters
	Node* obtenirParent() const;
	string obtenirChar() const;
	vector<Node*>& obtenirFils();

private:
	Node* parent_;//le parent
	string ceNode_;//le sommet d'ou on part
	vector<Node*> fils_;//tous les sommets partant du Node
};

#endif // !H_NODE
