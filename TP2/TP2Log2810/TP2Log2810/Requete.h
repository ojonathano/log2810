#pragma once

#include <string>
using namespace std;

class Requete {
public:
	Requete();
	Requete(string& cp1, string& cp2, int& poids); 

	string getCodePost1(); 
	string getCodePost2(); 
	int getPoids(); 
	bool estValide(); 
	void setValidite(const bool& v); 

	~Requete();

private:
	string codePost1_, codePost2_; 
	int poids_; 
	bool estValide_; 
};