#include "FileDeRequete.h"

/**
* Constructeur.
*/
FileDeRequete::FileDeRequete() {}

/**
* Destructeur.
*/
FileDeRequete::~FileDeRequete() {}

/**
* Permet de retourner une instance d'une file de requete.
*/
FileDeRequete* FileDeRequete::getInstance() {
	static FileDeRequete* INSTANCE = new FileDeRequete(); 
	return INSTANCE; 
}

/**
* Permet d'ajouter les requetes a la file de requete.
* @param r: La requete a ajouter.
*/
void FileDeRequete::ajouterRequete(Requete& r) {
	lesRequetes.push(r);
}

/**
* Permet d'avoir acces a la file de requetes.
*/
queue<Requete> FileDeRequete::obtenirRequete() {
	return lesRequetes; 
}

/**
* Permet de savoir si la file de requetes est vide.
*/
bool FileDeRequete::estVide() {
	return (lesRequetes.size() == 0); 
}

/**
* Permet de vider la file de requetes.
*/
void FileDeRequete::clear() {
	while (!lesRequetes.empty())
		lesRequetes.pop();
}