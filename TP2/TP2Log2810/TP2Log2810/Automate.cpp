#include "Automate.h"

using namespace std;

Automate::Automate()
{
	bool ok = lireFichier();
	racine_ = new Node;
	if (ok)
		creerArbreAdresses();
}

Node* Automate::getRacine() const {
	return racine_;
}

Automate::~Automate(){}

Automate* Automate::getInstance() {
	static Automate* INSTANCE = new Automate();
	return INSTANCE;
}

vector<string> Automate::getListe() {

	return listeAdresse_;
}

bool Automate::lireFichier() {
	cout << "Entrez le nom du fichier incluant son extension.  Ex: monFichier.txt" << endl;
	string nomFichier;
	cin >> nomFichier;
	if (nomFichier == "//") { nomFichier = "CodesPostaux.txt"; }//option d'utiliser le fichier par d�faut
	ifstream lire(nomFichier);
	while (lire.fail()) {
		cout << "la lecture echoue" << endl;
		cout << "Entrez le nom du fichier incluant son extension.  Ex: monFichier.txt" << endl;
		cin >> nomFichier;
		lire.open(nomFichier);
	}
	cout << "Fichier " << nomFichier << " trouve. Lecture..." << endl
		<< "================================================" << endl;
	uint8_t compteur = 0;
	string temp = "";
	while (!lire.eof()) {
		compteur++;
		getline(lire, temp);
		cout << " - " << temp;
		listeAdresse_.push_back(temp);
		if (compteur % 5 == 0) { cout << " - " << endl; }
	}
	cout << endl << "================================================" << endl << "Lecture finie." << endl;
	return true;
}

//ici l'implementation de cr�ation de l'arbre est inspir�e du parcours par niveau
void Automate::creerArbreAdresses() {

	//Queue pour la cr�ation par niveau de l'arbre
	queue<Node*> tempCreation;

	//Node actuelle pour la cr�ation de l'arbre
	Node* nodeActuelle = racine_;  // initialement == racine
	
	//Mettre la racine dans la queue
	tempCreation.push(nodeActuelle);

	//Pour les 6 lettres des codes postaux
	for (size_t i = 0; i < 6; i++) {

		//tant que la file n'est pas vide et que la longueur de la cgaine des nodes actuelles == i
		while ((nodeActuelle->obtenirChar().length() == i)){
			//boucle sur toutes les codes postaux mis en m�moire suite  a la lecture du fichier
			for (size_t j = 0; j < listeAdresse_.size(); j++) {

				//string temporaire contenant la string reconnue par la node + le caractere it�r�
				//Nous utilisons cela pour v�rifi� la validit� des caracteres pr�c�dents et la pr�sence du fils identique sur la node
				string temp = listeAdresse_[j].substr(0,i+1);
				
				//Si la s�quence pr�c�dente de la node correspond a la section de l'adresse v�rifi� , on v�rifie d'abors si un fils identique existe 
				//sinon, on l'ajoute
				bool filsExiste = false; // bool pour verifier si le fils existe
				if (nodeActuelle->obtenirChar() == listeAdresse_[j].substr(0, i)) {
					for (size_t k = 0; k < nodeActuelle->obtenirFils().size(); k++) {
						if (nodeActuelle->obtenirFils()[k]->obtenirChar() == temp) 
							filsExiste = true;
					}//endfor k
					//Si le fils n'existe pas, on le cr�� et l'ajoute
					if (!filsExiste) {
						nodeActuelle->obtenirFils().push_back(new Node(nodeActuelle, temp));
						//si la chaine a 6 caracteres elle est terminale et forme un quartier valide
						if (temp.length() == 6)
							listeSansDoublons_.push_back(temp);
					}
				}//endif
			}//endfor j

			//on ajoute tout les fils de node actuelle a la queue avant de pop node actuelle
			for (size_t z = 0; z < nodeActuelle->obtenirFils().size(); z++) {
				tempCreation.push(nodeActuelle->obtenirFils()[z]);

			}//endfor z
			
			tempCreation.pop();// on enleve l'�l�ment qui �tait le pr�c�dent node actuelle 
			nodeActuelle = tempCreation.front(); //node actuelle deviens l'�l�ment suivant de la file

		}//endwhile
	}//endfor i
}//end fonction

bool Automate::validerAdresse(string adresse) {
	bool requeteValide = true;
	bool filsTrouve;
	Node* noeudActuel = racine_;
	for (size_t i = 1; i < 7; i++) {
		filsTrouve = false;
		for (size_t j = 0; j < noeudActuel->obtenirFils().size() && filsTrouve == false; j++) {
			if (noeudActuel->obtenirFils()[j]->obtenirChar() == adresse.substr(0, i)) {
				filsTrouve = true;
				noeudActuel = noeudActuel->obtenirFils()[j];
			}//end if
		}//endfor j
		if (!filsTrouve)
			return false;
	}//endfor i
	return true;
}

bool Automate::validerRequete(Requete& r) {
	string cp1 = r.getCodePost1();
	string cp2 = r.getCodePost2();
	//v�rification triviales de validit� des string
	if ((cp1 == cp2) || cp1.length() != 6 || cp2.length() != 6) {
		r.setValidite(false);
		return false;
	}
	else
		return(validerAdresse(cp1) && validerAdresse(cp2));
}

void Automate::reinitialiserArbre(){
	delete racine_;
	racine_ = new Node;
	listeAdresse_.clear();
	lireFichier();
	creerArbreAdresses();
	listeSansDoublons_.clear(); 
}

vector<string>& Automate::getListeSansDoublons() {
	return listeSansDoublons_;
}
