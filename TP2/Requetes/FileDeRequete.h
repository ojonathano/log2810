#pragma once
#include "Requete.h"
#include <queue>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

class FileDeRequete
{
public:
	static FileDeRequete* getInstance(); 
	queue<Requete> obtenirRequete();
	void ajouterRequete(Requete& r);

	bool estVide(); 
	void clear(); 

	~FileDeRequete();
private:
	//Constructeur priv�, cette classe est un Singleton
	FileDeRequete();
	queue<Requete> lesRequetes;

};

