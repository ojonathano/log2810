#include "Node.h"
//constructeurs
Node::Node(): ceNode_(NULL), fils_(NULL) {}//par d�faut

Node::Node(const char& tN) : ceNode_(tN) {}

Node::Node(const char& tN, const std::vector<Node*>& fils) :ceNode_(tN), fils_(fils) {}

Node::Node(Node const& aCopier){
	ceNode_ = aCopier.obtenirChar();
	fils_ = aCopier.obtenirFils();
}

//destructeur
Node::~Node() {
	//par aggr�gation, donc pas de delete
}

//m�thodes
char Node::obtenirChar() const {
	return ceNode_;
}

std::vector<Node*> Node::obtenirFils() const {
	return fils_;
}

void Node::ajouterEnfant(Node* enfant) {
	fils_.push_back(enfant);
}