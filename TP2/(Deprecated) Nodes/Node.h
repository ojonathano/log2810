#ifndef H_NODE
#define H_NODE

/********************************
* name: IntelliNode.cpp
* date: 15 nov 2017
* desc: Fichier implémentant les Nodes utilisés dans l'automate
*********************************/

#include <vector>

class Node
{
public:
	//constructeurs
	Node();
	Node(const char& tN);
	Node(const char& tN, const std::vector<Node*>& fils);
	Node(const Node& aCopier);
	//destructeurs
	~Node();
	//getters
	char obtenirChar() const;
	std::vector<Node*> obtenirFils() const;
	//setters
	void ajouterEnfant(Node* enfant);

private:
	char ceNode_;//le sommet d'ou on part
	std::vector<Node*> fils_;//tous les sommets partant du Node
};

#endif // !H_NODE
