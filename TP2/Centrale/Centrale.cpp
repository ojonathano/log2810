#include "Centrale.h"
#pragma once

/**
* Constructeur
*/
Centrale::Centrale() {
	nbrRequetesInvalides = 0; 
	nbrRequetesTraites = 0; 
	creerFlote();
}

Centrale::Centrale(vector<string> adresse) {
	nbrRequetesInvalides = 0;
	nbrRequetesTraites = 0;
	creerFlote();
	creerQuartiers(adresse);
}
/**
* Permet de creer la liste des quartiers qui sera envoyer a la Centrale.
* @param fichiersQ: Fichiers contenant les codes postaux des quartiers.
*/
void Centrale::creerQuartiers(vector<string> adresse) {
	for (size_t i = 0; i < adresse.size(); i++) {
		Quartier* tmp = (new Quartier(adresse[i]));
		int reinitialiser = 0;
		tmp->setCyclesSD_(reinitialiser);
		lesQuartiers.push_back(tmp);
		}
}

/**
* Constructeur par parametre.
* @param adresse: Les adresses des quartiers.
*/
Centrale::Centrale(vector<string> adresse) {
	nbrRequetesInvalides = 0;
	nbrRequetesTraites = 0;
	creerFlote();
	creerQuartiers(adresse);
}

/**
* Creer la flotte de la compagnie, soit avec 5 drones de type 2, et 10 de type 1 
*/
void Centrale::creerFlote() {
	// 5 drones de type 2 
	for (int i = 0; i < 5; i++)
		flotte.push_back(new DroneC2());

	// 10 drones de type 1 
	for (int j = 0; j < 10; j++)
		flotte.push_back(new DroneC1());
}

/***
* Permet de reequilibrer la flotte apres chaque cycle de requtes.
*/
void Centrale::equilibrerFlotte() {
	// La liste des quartiers dont on attribuera les drones
	vector<Quartier*> tmpQu1 = lesQuartiers;
	// Liste qui sera parcouru pour aller chercher un drone
	vector<Quartier*> tmpQu2 = lesQuartiers;

	for (size_t i = 0; i < tmpQu1.size(); i++) {
		if (tmpQu1[i]->getCyclesSD_() >= 2) {
			for (size_t j = 0; j < tmpQu2.size(); j++) {
				if (tmpQu2[j]->getDrones().size() >= 2) {
					// S'il y a presence d'un drone dans le quartier parcouru, on le retire
					// du quartier et on l'envoie vers le prochain
					Drone* tmp = tmpQu2[j]->getDrones()[0];
					retirerDrone(tmp, *tmpQu2[j]);
					retirerDrone(tmp, *tmpQu1[i]);
					tmpQu1[i]->addDrones(tmp);
				}
			}
		}
	}
	setQuartier(tmpQu1);
}

/**
* Permet de deplacer un colis vers sa destination
* @param requete: La requete a traiter. 
*/
void Centrale::deplacerColis(Requete& requete) {
		int poidsConvertis = requete.getPoids() / 1000; 
		Drone* drone = assignerColis(poidsConvertis, requete.getCodePost1());

		// Deplacement du drone vers la destination
		if (drone != nullptr) {
			for (size_t i = 0; i < lesQuartiers.size(); i++) {
				if (lesQuartiers[i]->getAdresse() == requete.getCodePost2()) {
					retirerDrone(drone, *lesQuartiers[i]);
					lesQuartiers[i]->addDrones(drone);
				}
			}
			requete.setValidite(true);
			drone->setEstAssigner(false); 
			//cout << drone->getPoidsMax() << endl;
		}
		else if (drone == nullptr)
			requete.setValidite(false);
}

/**
* Permet de traiter les requetes qui sont stocker dans la file de requetes.
* @param requetes: La file de requetes en question.
* @param fichierR: Le fichier de requetes qui sera lu. 
*/
void Centrale::traiterLesRequetes(FileDeRequete& requetes, ifstream& fichierR, Automate& automate) {
	if (fichierR.eof())
		cout << "Fichier introuvable" << endl;

	else {
		while (!fichierR.eof()) {
			string line;
			getline(fichierR, line);

			string cp1, cp2;
			int poids = 0;
			stringstream ss(line);
			ss >> cp1 >> cp2 >> poids;

			Requete tmp(cp1, cp2, poids);
			// If --> pour la validation de la requete ...
			automate.validerRequete(tmp);
			requetes.ajouterRequete(tmp);
		}
	}

	// Traitement des requetes est reellement a partir d'ici
	queue<Requete> filetmp = requetes.obtenirRequete();
	while (!filetmp.empty()) {
		deplacerColis(filetmp.front());
		if (!filetmp.front().estValide() && filetmp.front().getPoids() != 0) {
			nbrRequetesInvalides++;
		}
		nbrRequetesTraites++; 
		filetmp.pop();
	}
	// Clear la file de requete passee en parametre pour recommencer un
	// nouveau cycle 
	incCentrale();
	requetes.clear();
}

/**
* Permet d'assigner un colis, selon son poids, a un drone.
* @param pt: Poids du colis qui sera transporte.
* @param adresseDep: Adresse de depart du colis. 
* @return Drone*: Retourne le drone qui est assigner au colis.
*/
Drone* Centrale::assignerColis(int& pt, string& adresseDep) {

	for (size_t i = 0; i < lesQuartiers.size(); i++) {
		if (lesQuartiers[i]->getAdresse() == adresseDep) {
			if (!lesQuartiers[i]->getDrones().empty()) {
				for (size_t j = 0; j < lesQuartiers[i]->getDrones().size(); j++) {
					if (!lesQuartiers[i]->getDrones()[j]->estAssigner() && lesQuartiers[i]->getDrones()[j]->getPoidsMax() > pt) {
						Drone* tmp = lesQuartiers[i]->getDrones()[j];
						retirerDrone(tmp, *lesQuartiers[i]);
						return tmp;
					}
				}
			}
		}
	}
	return nullptr;
}
/**
* Repartie la flotte de drones aleatoirement a la premiere execution
*/
void Centrale::repartirFlotte() {
	srand((unsigned int)time(NULL)); 
	for (size_t i = 0; i < flotte.size(); i++) {
		int randomInteger = (rand() % lesQuartiers.size() + 1) - 1;
		lesQuartiers[randomInteger]->addDrones(flotte[i]);
	}
}
/**
* Permet d'imprimer les statistiques. C-a-d, le nombres de requetes 
* traitees et le nombre de requetes invalides.
*/
void Centrale::imprimerStatistiques() {
	cout << "STATISTIQUES" << endl;
	cout << "------------" << endl;
	cout << "REQUETES TRAITES: " << getNbrRequetesTraites() << endl;
	cout << "REQUETES INVALIDES: " << getNbrRequetesInvalides() << endl;
}

/**
* Getter. Retourne le nombre de requetes traitees.
*/
int Centrale::getNbrRequetesTraites() {
	return nbrRequetesTraites; 
}

/**
* Getter. Retourne le nombre de requetes invalides.
*/
int Centrale::getNbrRequetesInvalides() {
	return nbrRequetesInvalides; 
}

/**
* Imprime la repartition de la flotte de drone sur tout le reseau.
*/
void Centrale::imprimerRepartitionFlotte() {
	cout << "------------------------" << endl;
	cout << "REPARTITION DE LA FLOTTE" << endl; 
	cout << "| " << "Quartier " << " | " << "Drone faible capacite" << " | " << " Drone forte capacite" << " | " << endl;
	for (size_t i = 0; i < lesQuartiers.size(); i++) {
		cout << setw(3) << " " << lesQuartiers[i]->getAdresse();

		int droneC1 = 0; 
		int droneC2 = 0; 
		for (size_t j = 0; j < lesQuartiers[i]->getDrones().size(); j++) {
			if (lesQuartiers[i]->getDrones()[j]->getPoidsMax() == 1) {
				droneC1++; 
			}
			else if (lesQuartiers[i]->getDrones()[j]->getPoidsMax() == 5) {
				droneC2++; 
			}
		}
		cout << " " << setw(15) << droneC1 << " " << setw(23) << droneC2 << endl; 
	}
}

/**
* Incremente le compteur de cycles sans drones a l'interieur de 
* chaque quartier
*/
void Centrale::incCentrale() {
	vector<Quartier*> tmp = lesQuartiers;
	for(size_t i = 0; i < tmp.size(); i++){
		if (tmp[i]->getDrones().empty())
			tmp[i]->incCyclesSD_();
	}
	setQuartier(tmp);
}

/**
* Retire un drone specifique a l'interieur d'un quartier.
* @param drone: Le drone a retirer.
* @param qu: Le quartier dans lequel il se trouve.
*/
void Centrale::retirerDrone(Drone* drone, Quartier& qu) {
	vector<Drone*> tmp = qu.getDrones(); 
	if(drone != nullptr) {
		vector<Drone*>::iterator itB = tmp.begin();
		vector<Drone*>::iterator itE = tmp.end();

		tmp.erase(remove(itB, itE, drone), itE);
		qu.setDrone(tmp);
	}
}

/**
* Permet de reinitialiser les quartiers.
* @param qu: Le vecteur de quartier redefinis.
*/
void Centrale::setQuartier(vector<Quartier*> qu) {
	lesQuartiers.clear();
	for (size_t i = 0; i < qu.size(); i++) {
		lesQuartiers.push_back(qu[i]); 
	}
}