#pragma once

//Quartier
#include "../Quartier/Quartier.h"

//Requetes
#include "../Requetes/Requete.h"
#include "../Requetes/FileDeRequete.h"

//Drones
#include "../Drones/Drones.h"
#include "../Drones/DroneC1.h"
#include "../Drones/DroneC2.h"

//Automate
#include "../Automate/Automate.h"

//g�n�ral
#include <vector>
#include <fstream>
#include <iostream>
#include <map>
#include <algorithm>
#include <stdio.h>      
#include <stdlib.h>   
#include <time.h>
#include <iomanip>
#include <algorithm>


using namespace std; 

class Centrale {
public: 
	
	Centrale(); 
	Centrale(vector<string> adresse);

	void creerQuartiers(vector<string> adresse);

	void equilibrerFlotte();
	void traiterLesRequetes(FileDeRequete& requetes, ifstream& fichierR, Automate& automate);

	//Seulement a la premier execution
	void repartirFlotte();

	void imprimerStatistiques();
	void imprimerRepartitionFlotte(); 

	int getNbrRequetesTraites();
	int getNbrRequetesInvalides();

private:

	void creerFlote();

	vector<Quartier*> lesQuartiers; 
	vector<Drone*> flotte; 

	int nbrRequetesTraites; 
	int nbrRequetesInvalides; 

	void setQuartier(vector<Quartier*> qu);
	void incCentrale();
	void retirerDrone(Drone* drone, Quartier& qu);
	void deplacerColis(Requete& requete);
	Drone* assignerColis(int& pt, string& adresseDep);
};

