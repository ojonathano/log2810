#include "Quartier.h"

/**
* Constructeur.
*/
Quartier::Quartier() {
	adresse = "unknown";
	cyclesSD_ = 0; 
}
/**
* Constructeur par parametre.
* @param adr: L'adresse du quartier.
*/
Quartier::Quartier(string& adr) {
	adresse = adr;
}

/**
* Getter. Permet de retourne l'adresse du quartier.
*/
string Quartier::getAdresse() {
	return adresse;
}

/**
* Permet de retourner la liste des drones a l'interieur du quartier.
*/
vector<Drone*> Quartier::getDrones() {
	return drones_;
}

/**
* Permet d'ajouter un drone a la liste des drones du quartier.
* @param drone: Le drone a ajouter. 
*/
void Quartier::addDrones(Drone* drone) {
	int reinitialiser = 0; 
	setCyclesSD_(reinitialiser);
	drones_.push_back(drone);
}

/**
* Permet de retourner le nombre de cycle sans drones.
*/
int Quartier::getCyclesSD_() {
	return cyclesSD_;
}

/**
* Permet de setter (reinitialiser) le nombre de cycles sans drones.
*/
void Quartier::setCyclesSD_(int& cycleCompteur) {
	cyclesSD_ = cycleCompteur; 
}

/**
* Permet d'incrementer le nombre de cycle sans drones.
*/
void Quartier::incCyclesSD_() {
	cyclesSD_++;
}

/**
* Permet de reinitialiser la liste de drone dans le quartier. 
*/
void Quartier::setDrone(vector<Drone*> drone) {
	drones_.clear();
	for (size_t i = 0; i < drone.size(); i++) {
		drones_.push_back(drone[i]);
	}
}