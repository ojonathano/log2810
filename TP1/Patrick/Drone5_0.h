#pragma once
#include "../Patrick/drone.h"

using namespace std;

class Drone5_0 :
	public Drone
{
public:
	Drone5_0();
	virtual ~Drone5_0();

	virtual float d�terminerPostDep(const int& chemin, const bool& rech);
	virtual void seDeplacer(const int& chemin, const bool& rech);
	virtual float determinerPerteAuto();
};

