#include "Drone.h"
#include "Colis.h"

Drone::Drone() : autonomie_{ 100 }, chargement_{nullptr}
{
}

Drone::~Drone()
{
}

//get!
float Drone::getAutonomie() {
	return autonomie_;
}

Colis* Drone::getChargement() {
	return chargement_;
}
//set
void Drone::setAutonomie(float newVal){
	autonomie_ = newVal;
}

void Drone::setChargement(Colis* newCol) {
	chargement_ = newCol;

}
