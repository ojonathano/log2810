#include "Drone5_0.h"


Drone5_0::Drone5_0(): Drone()
{
}


Drone5_0::~Drone5_0()
{
}

//selon le poids du chargement, renvoie le multiplicateur d'autonomie perdue par minute
float Drone5_0::determinerPerteAuto() {
	string temp = getChargement()->getPoids();
	if (temp == "plume")
		return 1;
	else if (temp == "moyen")
		return 1.5;
	else
		return 2.5;
}

//d�termine l'autonomie du drone apr�s le d�placement
float Drone5_0::d�terminerPostDep(const int& chemin, const bool& rech) {

	float nouvelleAutonomie = (getAutonomie() - (determinerPerteAuto() * chemin));
	//calcul de l'autonomie post-d�placement

	if (nouvelleAutonomie <= 0) {//fail, le drone tombera en chemin
		return 0;
	}
	if (rech) {//va se recharger l�, donc apr�s ce d�placement il va 
		return 100;
	}
	else {
		return nouvelleAutonomie;
	}
}

//effectue le d�placement (draine la batterie)
void Drone5_0::seDeplacer(const int& chemin, const bool& rech) {
	setAutonomie(d�terminerPostDep(chemin, rech));
}