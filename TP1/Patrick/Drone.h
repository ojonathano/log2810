/********************************
* name: Drone.h
* date: 17 oct 2017
* desc: Classe de base pour les drones
*********************************/
#ifndef DRONE_H
#define DRONE_H
#include "../Louis/DikjstraArcSommets/Arc.h"
#include "../Patrick/PoidsLourd.h"
#include "../Patrick/PoidsMoyen.h"
#include "../Patrick/PoidsPlume.h"
#include "../Patrick/Colis.h"

using namespace std;
class Drone
{
public:
	Drone();
	virtual ~Drone();

	//M�thode d�placement, calcule l'autonomie selon la distance parcourue et reduit autonomie
	virtual void seDeplacer(const int& chemin, const bool& rech) = 0;
	virtual float d�terminerPostDep(const int& chemin, const bool& rech) = 0;
	virtual float determinerPerteAuto() = 0;
	//get!
	float getAutonomie();
	Colis* getChargement();
	//set
	void setAutonomie(float newAut);
	void setChargement(Colis* newCol);

protected:
	//parametres
	float autonomie_;
	Colis* chargement_;
};

#endif
