#include "Drone3_3.h"


/*
La perte d�autonomie d�un drone 3.3 Amp�res va comme suit :
pour un colis de cat�gorie poids plumes, le quadricopt�re perd 10 % d�autonomie par 10 minutes de vol
.Pour un colis de cat�gorie poids moyens, le quadricopt�re perd 20 % d�autonomie par 10 minutes de vol
.Pour les colis de cat�gorie poids lourds, le quadricopt�re perd 40 % d�autonomie par 10 minutes de vol.
*/

Drone3_3::Drone3_3():Drone()
{
}


Drone3_3::~Drone3_3()
{
}

//selon le poids du chargement, renvoie le multiplicateur d'autonomie perdue par minute
float Drone3_3::determinerPerteAuto() {
	string temp = getChargement()->getPoids();
	if (temp == "plume")
		return 1;
	else if (temp == "moyen")
		return 2;
	else
		return 4;
}

//d�termine l'autonomie du drone apr�s le d�placement
float Drone3_3::d�terminerPostDep(const int& chemin, const bool& rech) {

	float nouvelleAutonomie = (getAutonomie() - (determinerPerteAuto() * chemin));
	//calcul de l'autonomie post-d�placement

	if (nouvelleAutonomie <= 0) {//fail, le drone tombera en chemin
		return 0;
	}
	if (rech) {//va se recharger l�, donc apr�s ce d�placement il va 
		return 100;
	}
	else {
		return nouvelleAutonomie;
	}
}

//effectue le d�placement (draine la batterie)
void Drone3_3::seDeplacer(const int& chemin, const bool& rech) {
	setAutonomie(d�terminerPostDep(chemin, rech));
}