#pragma once;

#include <iostream>
#include <string>
#include "Drone.h"
#include "../../repos/Louis/Algos/Dijkstra.h"
#include "../../repos/Patrick/Exo2/Graphe2.h"
using namespace std;

int main() {
	
	for(;;){
		cout << "*************************************************************************************************************" << endl;
		cout << "**                                                                                                          **" << endl;
		cout << "*                     Interface principale pour les fonctionnalites du TP1                                  *" << endl;
		cout << "*                                                                                                           *" << endl;
		cout << "*                          Par: Patrick St-Pierre                                                           *" << endl;
		cout << "*                                  Louis Serey                                                              *" << endl;
		cout << "*                                      Jonathan O'Neil                                                      *" << endl;
		cout << "*                                                                                                           *" << endl;
		cout << "**                                                                                                         **" << endl;
		cout << "*************************************************************************************************************" << endl;


		int reponse = 0;
		while (reponse != 1 && reponse != 2 && reponse != 3) {
			cout << "Trois options s'offrent presentement a vous, veuillez faire un choix maintenant" << endl;
			cout << "1: Acceder au menu de la section du programme Livraison par drones" << endl;
			cout << "2: Acceder au menu de la section du programme Desserts" << endl;
			cout << "3: Quitter le programme" << endl;
			cin >> reponse;
			if (reponse != 1 && reponse != 2 && reponse != 3)
				cout << "Entrez un choix valide !! " << endl;
		}
		switch(reponse) {


			//ce case implemente le menu pour la partie 1
				case 1: {
					int reponse1 = 0;
					Colis* colisLamb = nullptr;
					Graphe* grapheLamb = new Graphe;
					string nameFile = "";
					while (reponse1 != 3) {
					wcout << "                     /~~|                        * *  ________                 * * *		" << endl;
					wcout << "                  8-+  -|                      *   __(________)              *       *	" << endl;
					wcout << "                    |   |                      *     /~o~~~~~~\* *          *         *	" << endl;
					wcout << "                  8-+  -|      /~|              *   /_   ~~)~~~)    * *     *         *	" << endl;
					wcout << "               /----+   +-----/  |              *     |-    ~~/          *    *     *		" << endl;
					wcout << "              <  [- |   |   ======   * * *    *        ~~|   |              *  * * *  * *	" << endl;
					wcout << "               \----+   +-----\  |         **           /~~~~~\                           *" << endl;
					wcout << "                  8-+  -|      \ |        *  *         | \  \__|___ _||_                   *" << endl;
					wcout << "                    |   |       ~        *     *       |  \________<____>---------+        *" << endl;
					wcout << "                  8-+  -|                *     *        \_____/                            *" << endl;
					wcout << "                     \  |                  * *            | |                             *" << endl;
					wcout << "                      ~~                                  | |__                         *" << endl;
					wcout << "                                                          |____)     ** *  *  *    *" << endl << endl << endl;




					while (reponse1 != 1 && reponse1 != 2 && reponse1 != 3) {
						cout << "Trois options s'offrent presentement a vous, veuillez faire un choix maintenant" << endl;
						cout << "1: Mettre a jour la carte " << endl;
						cout << "2: Determiner le plus court chemin" << endl;
						cout << "3: quitter" << endl;
						cin >> reponse1;
						while (reponse1 != 1 && reponse1 != 2 && reponse1 != 3) {
							cout << "Entrez un choix valide !! " << endl;
							cin >> reponse1;
						}
					}
					switch (reponse1) {
					case 1: {
						cout << "entrez le nom du fichier contenant les informations de la nouvelle carte" << endl;
						cin >> nameFile;

						cout << "Debut du programme..." << endl << endl;
						ifstream fileTest(nameFile);
						streampos posFichier = 0;
						Graphe* grapheTest = new Graphe;
						grapheTest->creerGraphe(fileTest, posFichier);

						cout << "Affichage du graphe..." << endl;
						grapheTest->lireGraphe();

						grapheLamb = grapheTest;
						break;
					}

					case 2: {
						Dijkstra algo(grapheLamb);
						int som1 = 0;
						int som2 = 0;
						int poids = 0;
						cout << "inserez le numero du Sommet de depart" << endl;
						cin >> som1;
						while (som1 < 0 && som1 > grapheLamb->getSommets().size())
						{
							cout << "entree invalide, reecrivez le numero de sommet" << endl;
							cin >> som1;
						}

						cout << "inserez le numero du Sommet de d'arrivee" << endl;
						cin >> som2;
						while (som2 < 0 && som2 > grapheLamb->getSommets().size())
						{
							cout << "entree invalide, reecrivez le num�ro de sommet" << endl;
							cin >> som2;
						}

						cout << "choisissez le poids de votre colis : (1) leger (2) moyen (3) lourd" << endl;
						cin >> poids;
						while (poids < 1 && poids > 3)
						{
							cout << "entree invalide, reecrivez le choix de poids" << endl;
							cin >> poids;
						}

						switch (poids)
						{
						case 1:
							colisLamb = new PoidsPlume;
							break;
						case 2:
							colisLamb = new PoidsMoyen;
							break;
						case 3:
							colisLamb = new PoidsLourd;
							break;
						default:
							break;
						}
						cout << algo.findChemin(colisLamb, som1, som2);
						break;
					}

					case 3: {
						delete grapheLamb;
						grapheLamb = nullptr;
						delete colisLamb;
						colisLamb = nullptr;
						return 0;
						break;
					}
			
					}//fin du switch a linterieur du case 1
					reponse1 = 0;
				}
					break;
			}//fin case 1



			case 2: {
				int reponse2 = 0;
				while (reponse2 != 1 && reponse2 != 2 && reponse2 != 3) {
					cout << "Trois options s'offrent presentement a vous, veuillez faire un choix maintenant" << endl;
					cout << "1: Creer et afficher le graphe de recettes" << endl;
					cout << "2: Generer et afficher le diagramme de Hasse" << endl;
					cout << "3: Quitter le programme" << endl;
					cin >> reponse2;
					if (reponse2 != 1 && reponse2 != 2 && reponse2 != 3)
						cout << "Entrez un choix valide !! " << endl;
				}
					switch (reponse2) {
						case 1:{
							Graphe2 afficher;
							afficher.creerGrapheOriente("manger.txt");
						}
						case 2:{
							Graphe2 print;
							print.genererHasse();
							break;
						}
						case 3:{
							return 0;
						}
					}
					break;
				}
				

			
			case 3: 
				return 0;
		}
	}
};