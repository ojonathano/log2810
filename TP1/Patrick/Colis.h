#pragma once

#include <string>
using namespace std;

class Colis
{
public:
	//destructeur
	virtual ~Colis() {}
	//getters
	virtual string getPoids() =0;
};

