#ifndef POIDSPLUME_H
#define POIDSPLUME_H

#include <string>
#include "../Patrick/Colis.h"
class PoidsPlume :
	public Colis
{
public:
	PoidsPlume();
	~PoidsPlume();
	virtual string getPoids();

private:
	string poids_;
};

#endif