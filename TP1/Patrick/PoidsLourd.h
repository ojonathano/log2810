#ifndef POIDSLOURD_H
#define POIDSLOURD_H
#include "../Patrick/Colis.h"

class PoidsLourd :
	public Colis
{
public:
	PoidsLourd();

	~PoidsLourd();
	virtual string getPoids();

private:
	string poids_;
};

#endif