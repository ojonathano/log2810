#ifndef POIDSMOYEN_H
#define POIDSMOYEN_H

#include "../Patrick/Colis.h"
#include <string>

class PoidsMoyen :
	public Colis
{
public:
	PoidsMoyen();
	~PoidsMoyen();
	virtual string getPoids();


private:
	string poids_;
};

#endif