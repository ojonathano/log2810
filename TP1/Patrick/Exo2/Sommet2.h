/********************************
* name: Sommet2.h
* date: 11 oct 2017
* desc: classe pour  implémenter des sommets dans des graphes
*********************************/
#pragma once;
#include <vector>
#include <iostream>
#include <string>

using namespace std; 

class Sommet2 {
public: 
	//constructeurs-destructeurs
	Sommet2();
	Sommet2(string nom);
	~Sommet2();

	//getters
	vector<int> getNextSommets();
	vector<int> getPrevSommets();
	string getNom();
	bool getMinimum();
	bool getMaximum();

	//setters
	void setNom(string nom);
	void addNext(int next);
	void addPrecedent(int prev);
	void setMinimum(bool estMin);
	void setMaximum(bool estMax);

private:
	vector<int> nextSommets;
	vector<int> precedentsSommets;
	string nom_;
	bool estMinimum_;
	bool estMaximum_;
};

 