/********************************
* name: Sommet2.cpp
* date: 11 oct 2017
* desc: classe pour  implémenter des sommets dans le diagramme de Hasse
*********************************/
#pragma once;
#include "Sommet2.h"

//constucteur
Sommet2::Sommet2() {

}

//constructeur par paramètre
Sommet2::Sommet2(string nom) {
	nom_ = nom;
	estMinimum_ = true;
	estMaximum_ = false;
}

//destructeur
Sommet2::~Sommet2() {
}

//setters
void Sommet2::setMinimum(bool estMin) {
	estMinimum_ = estMin;
}
void Sommet2::setMaximum(bool estMax) {
	estMaximum_ = estMax;
}

void Sommet2::setNom(string nom) {
	nom_ = nom;
}
//getters
string Sommet2::getNom() {
	return nom_;
}
bool Sommet2::getMinimum() {
	return estMinimum_;
 }
bool Sommet2::getMaximum() {
	return estMaximum_;
}
void Sommet2::addNext(int next) {
	nextSommets.push_back(next);
}
vector<int> Sommet2::getNextSommets() {
	return nextSommets;
}
vector<int> Sommet2::getPrevSommets() {
	return precedentsSommets;
}
void Sommet2::addPrecedent(int prev) {
	precedentsSommets.push_back(prev);
}