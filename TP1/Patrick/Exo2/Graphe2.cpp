#pragma once;
#include "Graphe2.h"
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <cstdlib>// pour atoi convert string to int

/**
* Constructeur
*/
Graphe2::Graphe2() {

}

/**
* Destructeur
*/
Graphe2::~Graphe2() {
}
vector<Sommet2> Graphe2::getVecteur() {
	return lesSommets;
}

vector<vector<bool>> Graphe2::getMatriceHasse() {
	return matriceArcsHasse_;
}

vector<Sommet2> Graphe2::getLesSommets() {
	return lesSommets;
}

void Graphe2::afficherMatriceAdjacence() {
	//affiche l'indice de la colonne
	cout << "    1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 ..." << endl << endl;
	for (int i = 0; i < matriceAdjacence_.size(); i++) {
		
		//affiche l'indice de la rang�e
		if (i < 9)
			cout << i + 1 << ":  ";
		else
			cout << i + 1 << ": ";
		
		//affiche l'�l�ment
		for (int j = 0; j < matriceArcsHasse_.size(); j++) {
			cout << matriceAdjacence_[i][j] << "  ";
		}
		cout << endl;
	}
}

void Graphe2::afficherMatriceArcsHasse() {
	//affiche l'indice de la colonne
	cout << "    1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 " << endl << endl;
	for (int i = 0; i < matriceArcsHasse_.size(); i++) {

		//affiche l'indice de la rang�e
		if (i < 9)
			cout << i + 1 << ":  ";
		else
			cout << i + 1 << ": ";

		//affiche l'�l�ment
		for (int j = 0; j < matriceArcsHasse_.size(); j++) {
			cout << matriceArcsHasse_[i][j] << "  ";
		}
		cout << endl;
	}
}

void Graphe2::enleverBoucles() {
	for (int i = 0; i < matriceArcsHasse_.size(); i++) {
		if (matriceArcsHasse_[i][i] == true)
			matriceArcsHasse_[i][i] = false;
	}
}

void Graphe2::enleverTransitivite() {
	for (int j = 0; j < matriceArcsHasse_.size(); ++j)
		for (int i = 0; i < matriceArcsHasse_.size(); ++i)
			if (matriceArcsHasse_[i][j])
				for (int k = 0; k < matriceArcsHasse_.size(); ++k)
					if (matriceArcsHasse_[j][k]) {
						matriceArcsHasse_[i][k] = false;
					}
}

void Graphe2::lireFichier(ifstream& manger) {

	string read = "notempty"; 
	bool ligneBlanche = false; 
	bool matriceInitialisee = false;
	lesSommets.clear();

	while (!manger.eof()) {
		getline(manger, read);

		if (read.empty())
			ligneBlanche = true; 

		if (!ligneBlanche) {
			stringstream ss(read);

			getline(ss, read, ',');
			getline(ss, read);

			//cr�e un nouvel objet qui a conne nom la string extraite
			Sommet2 nouvS(read);
			lesSommets.push_back(nouvS);
		}

		//maintenant que le nombre d'�l�ments est connu, on initialise les vecteurs de la matrice
		//L'initialisation produit une matrice carr�e de bool�ens == false par d�faut
		if (ligneBlanche && !matriceInitialisee) {
			if (!matriceAdjacence_.empty())
				matriceAdjacence_.clear();
			if (!matriceArcsHasse_.empty())
				matriceArcsHasse_.clear();

			vector<bool>  temp;
			for (int i = 0; i < lesSommets.size(); i++) {
				temp.push_back(0);
			}
			for (int i = 0; i < lesSommets.size(); i++) {
				matriceAdjacence_.push_back(temp);
				matriceArcsHasse_.push_back(temp);
			}
			matriceInitialisee = true;
		}

		if (ligneBlanche) {
			//cas apres la ligne vide
			if (read.empty())
				getline(manger, read);
			stringstream ss(read);
			string d, f;
			getline(ss,d , ',');
			getline(ss, f);
			int debut = stoi(d);
			int fin = stoi(f);
			matriceArcsHasse_[debut-1][fin-1] = true;
			matriceAdjacence_[debut - 1][fin - 1] = true;
		}
	}
}
//dans cette m�thode , les sommets du vecteurs de sommets seront reli�s 
//cette proc�dure utilise la matrice arcs hasse qui doit au pr�alable etre g�n�r�e
void Graphe2::construireGraphe() {
	for (int i = 0; i < lesSommets.size(); i++) {
		for (int j = 0; j < lesSommets.size(); j++){
			if (matriceArcsHasse_[i][j] == true) {
				lesSommets[i].addNext(j);
				lesSommets[j].setMinimum(false);
				lesSommets[j].addPrecedent(i);
			}
		}
	}
	//maintenant que les somets sont reli�s, tout les sommets n'ayant pas de next sont par d�finition , des maximums
	for (int i = 0; i < lesSommets.size(); i++) {
		if (lesSommets[i].getNextSommets().size() == 0)
			lesSommets[i].setMaximum(true);
	}
}
void Graphe2::creerGrapheOriente(string nom) {
	ifstream lecture;
	lecture.open(nom);
	if (lecture.fail())
		cout << "La lecture echoue malheureusement, maintenant il est conseille d'aller pleurer votre vie au local M-1120" << endl;
	else {
		lireFichier(lecture);
		construireGraphe();
		for (int i = 0; i < lesSommets.size(); i++) {
			cout << "Sommet: " << i + 1 << "  " <<lesSommets[i].getNom() << " " << "a comme voisins les sommets suivants: ";
			for (int j = 0; j < lesSommets[i].getNextSommets().size(); j++) {
				cout << lesSommets[lesSommets[i].getNextSommets()[j]].getNom() << "  ";
			}
			for (int k = 0; k < lesSommets[i].getPrevSommets().size(); k++) {
				cout << lesSommets[lesSommets[i].getPrevSommets()[k]].getNom() << "  ";
			}
			cout << endl << endl;
		}
	}
}

void Graphe2::resoudreHasse(int sommet, string parcouru) {
	int nbNexts = lesSommets[sommet].getNextSommets().size();
	if (lesSommets[sommet].getMaximum())
		cout << parcouru << endl;
	for (int i = 0; i < nbNexts; i++) {
		
		string parcours = (parcouru += lesSommets[lesSommets[sommet].getNextSommets()[i]].getNom() );
		if (!(lesSommets[lesSommets[sommet].getNextSommets()[i]].getMaximum())) {
			parcours += "  ";
			resoudreHasse(lesSommets[sommet].getNextSommets()[i], parcours);
		}
		else {
			cout << parcours << endl;
		}
		parcours = parcouru += "  ";
	}
}
void Graphe2::genererHasse() {
	ifstream lecture;
	lecture.open("manger.txt");
	lireFichier(lecture);
	enleverTransitivite();
	enleverBoucles();
	construireGraphe();

	for (int i = 0; i < lesSommets.size(); i++) {
		if (lesSommets[i].getMinimum()) {
			resoudreHasse(i, (lesSommets[i].getNom() += "  "));
		}
	}
	//verifier lordre, des que un sommet a plus d'un next, ordre partiel
	bool ordreTotal = true;
	while (ordreTotal) {
		for (int i = 0; i < lesSommets.size(); i++) {
			if (lesSommets[i].getNextSommets().size() > 1)
				ordreTotal = false;
		}
	}
	cout << endl << "Ce graphe est d'ordre  " ;
	if (ordreTotal)
		cout << "total" << endl << endl;
	else
		cout << "partiel" << endl << endl;
}