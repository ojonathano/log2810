#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <iterator>
#include <sstream>
#include "Sommet2.h"
#include "Graphe2.h"

using namespace std; 




int main() {


	vector<Sommet2> graphe;
	ifstream manger;
	manger.open("manger.txt");
	if (manger.fail())
		cout << "failing" << endl;


	while (!manger.eof()) {

		bool vecteurPlein = false;
		string pass;
		string nommer;
		if (!vecteurPlein) {
			getline(manger, pass, ',');
			getline(manger, nommer);
			Sommet2 ajouter(nommer);
			if (!pass.empty() && !nommer.empty())
				graphe.push_back(ajouter);
			else vecteurPlein = true;
			
		}
	}
	
	
	vector<string> sommets= { "Dejeuner", "Desserts", "Confiture de rhubarbes", "Pate brisee", "Rhubarbes", "Fraises",
		"Zeste de citron", "Kombucha", "Citron", "Gelatine", "Mousse au citron", "Oeufs", "creme", "�ufs brouilles", "Tarte fraises - rhubarbes",
		"Tarte au Citron", "Lait", "Sucre", "Yoghurt", "Farine", "Poivre", "Sel" };
	

	vector<pair<int, int>> arretes;
	arretes.push_back(make_pair(3, 1));
	arretes.push_back(make_pair(17, 19));
	arretes.push_back(make_pair(19, 1));
	arretes.push_back(make_pair(10, 11));
	arretes.push_back(make_pair(9, 7));
	arretes.push_back(make_pair(7, 11));
	arretes.push_back(make_pair(11,16));
	arretes.push_back(make_pair(16,2));
	arretes.push_back(make_pair(20,4));
	arretes.push_back(make_pair(4,16));
	arretes.push_back(make_pair(21,14));
	arretes.push_back(make_pair(22,14));
	arretes.push_back(make_pair(12,14));
	arretes.push_back(make_pair(18,3));
	arretes.push_back(make_pair(18,11));
	arretes.push_back(make_pair(18,8));
	arretes.push_back(make_pair(8,1));
	arretes.push_back(make_pair(8,8));
	arretes.push_back(make_pair(5,8));
	arretes.push_back(make_pair(5,3));
	arretes.push_back(make_pair(5,15));
	arretes.push_back(make_pair(15,2));
	arretes.push_back(make_pair(16,2));
	arretes.push_back(make_pair(14, 2));
	arretes.push_back(make_pair(17, 14));
	arretes.push_back(make_pair(17, 13));
	arretes.push_back(make_pair(4, 5));
	arretes.push_back(make_pair(13, 15));
	arretes.push_back(make_pair(19, 1));
	arretes.push_back(make_pair(19, 11));

	ifstream lire;
	lire.open("manger.txt");

	string nom = "manger.txt";
	
	Graphe2 newGraphe;
	
	
	newGraphe.lireFichier(lire);
	newGraphe.enleverTransitivite();
	newGraphe.enleverBoucles();
	newGraphe.construireGraphe();
	newGraphe.genererHasse();
	newGraphe.afficherMatriceArcsHasse();
	for (int i = 0; i < newGraphe.getLesSommets().size(); i++)
		cout << newGraphe.getLesSommets()[i].getMinimum() << " ";
	cout << endl << newGraphe.getLesSommets()[1].getMaximum() << endl;

	

	system("pause");
}