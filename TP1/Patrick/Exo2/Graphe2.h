#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "Sommet2.h"


using namespace std; 

class Graphe2 {
public:
	// Constructeur et desctructeur
	Graphe2();
	~Graphe2(); 
	
	//Prend en parametre un vecteur de paires repr�sentant les arcs orient�s  .first ===> .second
	//void remplirMatrices(vector<pair<int, int>>);

	//affiche la matrice d'ajacence du graphe
	void afficherMatriceAdjacence();
	void afficherMatriceArcsHasse();

	//enleve les boucles sur la matrice Arc Hasse
	void enleverBoucles();
	//enleve les arcs transitifs dans la matrice Arcs Hasse
	//il ne semble pas avoir de transitivit� simple dans le graphe, cependant cette m�thode devrait traiter 
	//les cas de transitivit� s'il y en avait
	void enleverTransitivite();

	void lireFichier(ifstream& manger);
	void construireGraphe();
	
	//M�thode demand�e pour afficher le graphe
	void creerGrapheOriente(string nom);
	//M�thode demand�e pour afficher le diagramme de hasse
	void genererHasse();

	void resoudreHasse(int sommet, string parcouru);

	vector<Sommet2> getLesSommets();
	vector<Sommet2> getVecteur();
	vector<vector<bool>> getMatriceHasse();

private:

	vector<Sommet2> lesSommets;
	//***S'applique aux deux matrices:**** ROW == sommet depart COLUMN == sommet destination
	//Matrice d'adjacence, la taille d'initialisation devra etre revu afin de rendre son initialisation g�n�rale
	vector<vector<bool>> matriceAdjacence_;
	//Matrice utilis�e pour le traitement n�c�ssaire pour g�n�rer un diagramme de hasse
	vector<vector<bool>> matriceArcsHasse_;
};




