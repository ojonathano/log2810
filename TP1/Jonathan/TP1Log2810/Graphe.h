#pragma once
#ifndef _GRAPHES_H_
#define _GRAPHES_H_

#include <iostream>
#include <fstream>
#include "../../Louis/DikjstraArcSommets/Arc.h"
#include "../../Louis/DikjstraArcSommets/Sommet.h"
#include <string>
#include <vector>
#include <sstream>

const int INITIALE_SIZE = 10; 

using namespace std; 

class Graphe {
public:
	// Constructeur et desctructeur
	Graphe();
	~Graphe(); 
	
	//Methode de creation te de lecture du graphe
	void creerGraphe(ifstream& file, streampos& posFichier);
	void lireGraphe(); 

	//Methode getter 
	vector<Sommet*> getSommets();
	vector<Arc*> getArcs();

private: 
	// Attributs prives pour le graphe 
	vector<Arc*> lesArcs_; 
	vector<Sommet*> lesSommets_;

	//Methodes privees, seulement utilisees au sein de la classe
	vector<int> parse(ifstream& file, streampos& posDernierSommet);
	Sommet* trouverSommet(int& num); 
};

#endif


