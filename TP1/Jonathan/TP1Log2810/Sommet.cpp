/********************************
* name: Sommet.cpp
* date: 11 oct 2017
* desc: classe pour  implémenter des sommets dans des graphes
*********************************/

#include "Sommet.h"

//constucteur
Sommet::Sommet() {
	cityNum = 0;
	cityRecharge = 0;
}

//constructeur par paramètre
Sommet::Sommet(int& num, int& rech) {
	cityNum = num;
	cityRecharge = rech;
}

//destructeur
Sommet::~Sommet() {
}

//setters
void Sommet::setCityNum(int& num) {
	cityNum = num;
}

void Sommet::setCityRecharge(bool& rech) {
	cityRecharge = rech;
}

//getters
int Sommet::getCityNum() {
	return cityNum;
}

bool Sommet::getCityRecharge() {
	return cityRecharge;
}

