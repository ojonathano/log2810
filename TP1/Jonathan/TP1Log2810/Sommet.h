/********************************
* name: Sommet.h
* date: 11 oct 2017
* desc: classe pour  implémenter des sommets dans des graphes
*********************************/

#pragma once 
#ifndef _SOMMET_H_
#define _SOMMET_H_

const int INITIAL_SIZE = 10;

#include <iostream>

using namespace std;

class Sommet {
public:
	//constructeurs-destructeurs
	Sommet();
	Sommet(int& num, int& rech);
	~Sommet();
	//getters
	int getCityNum();
	bool getCityRecharge();
	//setters
	void setCityNum(int& num);
	void setCityRecharge(bool& rech);

private:
	int cityNum;
	bool cityRecharge;
};

#endif; 