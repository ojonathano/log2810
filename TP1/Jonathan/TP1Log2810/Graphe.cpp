#include "Graphe.h"
#define CONS_BAR "============================================================================="
#define CONS_OPEN "( "
#define CONS_CLOSE " )"
/**
* Constructeur
*/
Graphe::Graphe() {
}

/**
* Destructeur
*/
Graphe::~Graphe() {
	//composition par pointeur, donc...
	//clear des sommets et arcs
	lesArcs_.clear();
	lesSommets_.clear();
}

/**
* Methode qui permet de creer le graphe dont les sommets et les arcs se retrouvent dans le
* fichier dont on fait la lecture. La methode est recursive. 
* @param file: Le fichier qu'on passe en parametre a la methode. 
* @param posFichier: La position de la derniere ligne lue dans le fichier. 
*/

void Graphe::creerGraphe(ifstream& file, streampos& posFichier) {
	file.seekg(posFichier); 

	int compteurS = 0;
	int compteurA = 0; 

	if(!file.eof()) {
		vector<int> line = parse(file, posFichier);
		if (line.size() == 2) {
			Sommet* nouvS = new Sommet(line[compteurS++], line[compteurS]);
			lesSommets_.push_back(nouvS);
			creerGraphe(file, posFichier); 
		}
		if (line.empty())
			line = parse(file, posFichier);

		if (line.size() == 3) {
			Sommet* s1 = trouverSommet(line[compteurA++]);
			Sommet* s2 = trouverSommet(line[compteurA++]);
			int temps = line[compteurA];
			Arc* nouvA = new Arc(s1, s2, temps);
			lesArcs_.push_back(nouvA);
			creerGraphe(file, posFichier);
			}
		}
	}

/**
* Methode qui permet d'afficher le graphe selon chaque sommets. Celle-ci parcours d'abord
* le vecteur contenant les sommets pour ensuite les compares au sommet S1 du premier arc
* trouve dans le vecteur contenant les arcs. 
*/
void Graphe::lireGraphe() {
	for (size_t i = 0; i < lesSommets_.size(); i++) {
		int compteur = 1; 
		cout << CONS_BAR << endl;
		cout << "Sommet: " << CONS_OPEN << i + 1 << CONS_CLOSE << endl;
		cout << CONS_BAR << endl;
		for (size_t j = 0; j < lesArcs_.size(); j++) {
			if (lesSommets_[i] == lesArcs_[j]->getS1()) {
				cout << "Voisin " << compteur++ << ": " << CONS_OPEN << lesArcs_[j]->getS2()->getCityNum() << CONS_CLOSE << endl; 
				cout << "Distance: " << lesArcs_[j]->getTemps() << " minutes" << endl;
				cout << endl; 
			}
			else if (lesSommets_[i] == lesArcs_[j]->getS2()) {
				cout << "Voisin " << compteur++ << ": " << CONS_OPEN << lesArcs_[j]->getS1()->getCityNum() << CONS_CLOSE << endl;
				cout << "Distance: " << lesArcs_[j]->getTemps() << " minutes" << endl;
				cout << endl; 
			}
		}
		cout << endl; 
	}
}

/**
* Methode qui retourne le vecteur contenant les sommets.
* @return lesSommets_: Le vecteur en attribut de la classe. 
*/
vector<Sommet*> Graphe::getSommets() {
	return lesSommets_; 
}

/**
* Methode qui retourne le vecteur contenant les arcs. 
* @return lesArcs_: Le vecteur en attribut de la classe. 
*/
vector<Arc*> Graphe::getArcs() {
	return lesArcs_; 
}

/**
*		(Methode privee) 
* Methode qui permet de parser chaque ligne lue de facon a ignorer les caracteres ','
* @param file: Le fichier dont sont lue chaque ligne.
* @param posDernierSommet: La position dans le fichier de la derniere lue qui a ete
* lue.
* @return lines: La ligne parser.
*/
vector<int> Graphe::parse(ifstream& file, streampos& posDernierSommet) {

	vector<int> lines;
	string read;
	getline(file, read);
	stringstream ss(read);

	int i;
	/**Boucle qui permet de d'ignorer le caractere ',' dans le string */
	while (ss >> i) {
		if (ss.peek() == ',') {
			ss.ignore();
		}
		lines.push_back(i);
	}
	/**Position de la derniere ligne lue */
	posDernierSommet = file.tellg();
	return lines;
}

/**
*		(Methode privee) 
* Methode qui permet de trouver un sommet selon le numero de ville attribue
* @param num: Le numero de la ville du sommet que l'on desire
* @return le sommet en questions. 
*/
Sommet* Graphe::trouverSommet(int& num) {
	for (size_t i = 0; i < lesSommets_.size(); i++) {
		if (lesSommets_[i]->getCityNum() == num)
			return lesSommets_[i]; 
	}
		return nullptr;
}


