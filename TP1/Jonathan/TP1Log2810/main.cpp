#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <iterator>
#include <sstream>

#include "../../Louis/Algos/Dijkstra.h"

using namespace std; 

int main() {

	cout << "Debut du programme..." << endl << endl;
	string nameFile = "arrondissements.txt";
	ifstream fileTest(nameFile);
	PoidsLourd* charge = new PoidsLourd;
	streampos posFichier;

	Graphe* grapheTest = new Graphe;
	
	grapheTest->creerGraphe(fileTest, posFichier); 

	cout << "Affichage du graphe..." << endl;
	//grapheTest->lireGraphe(); 

	Dijkstra algo(grapheTest); 
	cout << algo.findChemin(charge, 1, 19) << endl;

	delete grapheTest;
	grapheTest = nullptr;

	delete charge;
	charge = nullptr;
}