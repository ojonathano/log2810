#include "Arc.h"
/********************************
* name: Arc.h
* date: 11 oct 2017
* desc: Classe impl�mentant les arcs d'un graphe
*		l'arc peux autant �tre trait� comme un arc orient� que non
*		orient�:		(sommet1)-=(temps)=>(sommet2)
*		non-orient�:	(sommet1)-=(temps)=-(sommet2)
*********************************/

//Constructeur
Arc::Arc() {
	sommet1 = nullptr;
	sommet2 = nullptr;
	temps = 0;
}

//constructeur par param�tre
Arc::Arc(Sommet* s1, Sommet* s2, int t) {
	sommet1 = s1;
	sommet2 = s2;
	temps = t;
}

//destructeur
Arc::~Arc() {
	delete sommet1, sommet2; 
}

//setters
void Arc::setS1(Sommet* s1) {
	sommet1 = s1;
}

void Arc::setS2(Sommet* s2) {
	sommet2 = s2;
}

void Arc::setTemps(int t) {
	temps = t;
}

//getters
Sommet* Arc::getS1() {
	if (sommet1 == nullptr)
		return nullptr; 

	return sommet1;
}

Sommet* Arc::getS2() {
	if (sommet2 == nullptr)
		return nullptr; 

	return sommet2;
}

int Arc::getTemps() {
	return temps;
}