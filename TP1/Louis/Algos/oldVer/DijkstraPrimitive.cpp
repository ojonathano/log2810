/********************************
* name: Algo.cpp
* date: 18 oct 2017
* desc: Fichier impl�mentant les algorithmes utilis�s dans le tp soit:
*		une variation de l'algorithme de Dijkstra
*		un algorithme pour cr�er un diagramme de Hasse
*********************************/

#include "../../Jonathan/TP1Log2810/Graphe.h"
#include "../../TestBuild/Arc.h"
#include "../../TestBuild/Sommet.h"
#include "../DikjstraArcSommets/IntelliNode.h"
#include <string>
#include <algorithm>

#define INFINITY -1

using namespace std;

//fonction qui trouve le Node voulu dans une collection d'IntelliNodes
IntelliNode* FindNode(vector<IntelliNode>& Nodes, Sommet* sommet) {
	return find_if(Nodes.begin(), Nodes.end(), [sommet](IntelliNode n) { return n.getNode() == sommet; })._Ptr;
}

//fonction qui teste si le sommet se retrouve dans S
bool dansS(const Sommet* sommet, const Sommet** S,const int& nbS) {
	for (int i = 0; i < nbS; i++)
	{
		if (S[i] == sommet)
			return true;
	}
	return false;
}



void iterateUn(Sommet* activeSom, vector<IntelliNode>& Nodes, Sommet** s, int& nbS) {
	Arc* meilleurArc = new Arc(nullptr, nullptr, INFINITY);//cr�ation d'un arc bidon/tampon

	//update du s et nbs

	//intellinode pour les voisins, on update les voisins
	for each (Arc* var in FindNode(Nodes, activeSom)->getArcs()) {
		//apr�s avoir trouv� le bon IntelliNode correspondant au sommet regard� dans le tableau, on regarde chacun de ses arcs.
		if ( (meilleurArc->getTemps() == INFINITY || var->getTemps() < meilleurArc->getTemps() )//fin de la condition OU
			&& (!dansS(var->getS2(), s, nbS) )//fin de la condition ET
			) //donc, si INFINITY ou plus petit temps tout en n'ayant pas ecore �t� observ�
		{
			meilleurArc->setTemps(var->getTemps()); //comparer les temps entre arcs partant de cet IntelliNode et choisir le <
		}
	}

	//on regarde le plus court chemin depuis le chemin de d�part

	//on le choisit et on change le sommet actif

	

	delete meilleurArc;//delete 
	meilleurArc = nullptr;
}


/*
		ALGORITHME PRINCIPAL DIJKSTRA
		utilise les diff�rentes fonctions pour  trouver le chemin le plus

*/
string dikjstra(Graphe* g, const int depart, const int arrivee) {
	//cr�er les IntelliNodes
	const int nbSommets = g->getSommets().size();
	vector<IntelliNode> Nodes;
	for (int i = 0; i < nbSommets; i++)
	{
		Nodes.push_back(IntelliNode(g->getSommets()[i], g));
	}
	Sommet* activeSom = g->getSommets()[depart-1];//r�cup�ration du sommet de d�part

	//d�claration des tabeaux dynamiques 
	int* distChemins = new int[nbSommets]; //d�claration du vecteur de chemins courts
	string* parcours = new string[nbSommets]; //d�claration du vecteur de parcours
	Sommet** s = new Sommet*[nbSommets];//d�claration des S sommets parcourus
	int nbS = 0;//compteur du nombre de sommets �valu�s

	for (size_t i = 0; i < nbSommets; i++)//d�finition du size des deux vecteurs
	{
		distChemins[i] = INFINITY;
		parcours[i] = to_string(NULL);
	}

	//d�finition du point de d�part
	distChemins[activeSom->getCityNum() - 1] = 0;//set la distance � 0 pour le d�part
	parcours[activeSom->getCityNum() - 1] = activeSom->getCityNum() + " ";//set le chemin parcouru � lui m�me pour le d�part

	while (activeSom != g->getSommets()[arrivee - 1])//tant que le noeud observ� n'est pas le noeud d'arriv�e
	{
		//une it�ration
		iterateUn(activeSom, Nodes, s, nbS);

	}

	//delete des pointeurs cr�es
	delete distChemins;
	distChemins = nullptr;
	delete parcours;
	parcours = nullptr;
	delete s;
	s = nullptr;

	return "swag in the house";
}

int main() {

	Graphe g;
	


}