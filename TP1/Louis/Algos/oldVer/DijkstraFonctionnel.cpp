#include "Dijkstra.h"
const static string PROMPT = "Le chemin pour se rendre au sommet: ";
#define INFINI -1

//constructeur par d�faut ===============================================================================================
Dijkstra::Dijkstra()
{
	g_ = new Graphe();
	nbSommets_ = NULL;
	Nodes_ = new vector<IntelliNode>;
	activeSom_ = nullptr;
	distChemins_ = nullptr;
	parcours_ = nullptr;
	s_ = nullptr;
	int nbS_ = NULL;
}

//constructeur par param�tre ============================================================================================
Dijkstra::Dijkstra(Graphe* g)
{
	g_ = g;
	nbSommets_ = g->getSommets().size();
	Nodes_ = new vector<IntelliNode>;
	for (int i = 0; i < nbSommets_; i++){
		Nodes_->push_back(IntelliNode(g->getSommets()[i], g));
	}
	activeSom_ = nullptr;
	distChemins_ = new int[nbSommets_];
	parcours_ = new string[nbSommets_];
	s_ = new Sommet*[nbSommets_];
	int nbS_ = NULL;
}

//destructeur ===========================================================================================================
Dijkstra::~Dijkstra()
{
	//on delete pas le g car par aggr�gation
	//delete du vecteur apr�s l'avoir vid�
	Nodes_->clear();
	delete Nodes_;
	Nodes_ = nullptr;
	//on delete pas le activeSom, car par aggr�gation
	//delete des tableaux d'information
	delete[] distChemins_;
	distChemins_ = nullptr;
	delete[] parcours_;
	distChemins_ = nullptr;
	delete[] s_;//juste le conteneur, les Sommets* �taient par aggr�gation
	distChemins_ = nullptr;
}

//getters ===============================================================================================================
/*
Fonction qui renvoie la distance la plus courte vers un node donn�
au moment pr�sent des it�rations de l'algo de Dijkstra
�tant donn� que le tableau des sommets est tri� (car le fichier lui m�me l'est)
tous les tableaux qui en d�coulent sont tri�s aussi et donc acc�der � la case
#[num�ro de sommet]-1 acc�de � la distance
*/
int Dijkstra::getDistVers(Sommet* s) {
	return distChemins_[s->getCityNum() - 1];
}

//fonction qui renvoie le chemin le plus court connu au moment pr�sent des it�rations de l'algo de Dijkstra
string Dijkstra::getCheminVers(Sommet*s) {
	return parcours_[s->getCityNum() - 1];
}

//setters ==============================================================================================================
//set un nouveau sommet actif
void Dijkstra::setActiveSommet(Sommet* a) {
	activeSom_ = a;
}

//ajoute un graph � l'objet et toutes les informations qui en d�coulent
void Dijkstra::setGraph(Graphe* g) {
	g_ = g;
	nbSommets_ = g->getSommets().size();
	for (int i = 0; i < nbSommets_; i++) {
		Nodes_->push_back(IntelliNode(g->getSommets()[i], g));
	}
	distChemins_ = new int[nbSommets_];
	parcours_ = new string[nbSommets_];
	s_ = new Sommet*[nbSommets_];
}

//fonction qui assigne un nouveau poids � un parcours vers un Sommet depuis le point de d�part
void Dijkstra::setDistVers(Sommet* s, int d) {
	distChemins_[s->getCityNum() - 1] = d;
}

//fonction qui assigne un nouveau parcours vers un Sommet depuis le point de d�part
void Dijkstra::setCheminVers(Sommet*s, string c) {
	parcours_[s->getCityNum() - 1] = c;
}

// fonctions de traitement ============================================================================================

//fonction qui renvoie un pointeur vers le Node voulu dans une collection d'IntelliNodes
IntelliNode* Dijkstra::FindNode(const Sommet* sommet) {
	return find_if(Nodes_->begin(), Nodes_->end(), [sommet](IntelliNode n) { return n.getNode() == sommet; })._Ptr;
}

//fonction qui d�termine si le Sommet donn� est dans le tableau s
bool Dijkstra::dansS(const Sommet* sommet) {
	for (int i = 0; i < nbS_; i++)
	{
		if (s_[i] == sommet)
			return true;
	}
	return false;
}

int Dijkstra::FindDistMin() {
	int min = INFINI;
	int indexMin = 0;
	for (int i = 0; i < nbSommets_; i++) {
		//verifier que cet index n'est pas dans S


		if ( (distChemins_[i] < min || min == INFINI) && (!dansS(g_->getSommets()[i]) ) && (distChemins_[i]!= INFINI) ) {
			min = distChemins_[i];
			indexMin = i;
		}
	}
	return indexMin;
}

void Dijkstra::updateVoisins() {
	//intellinode pour les voisins, on update les voisins
	for each (Arc* var in FindNode(activeSom_)->getArcs()) {
		//apr�s avoir trouv� le bon IntelliNode correspondant au sommet regard� dans le tableau, on regarde chacun de ses arcs.
		Sommet* dest = var->getS2();

		if ( ( (getDistVers(dest) == INFINI) || (getDistVers(activeSom_) + var->getTemps() < getDistVers(dest) ) )/*fin du OU*/
			// (Infini < nouv chemin direct) ou chemin vers activeSom + le temps pour traverser l'arc var < le temps d�ja connu 
			&& (!dansS(dest) )/*fin du ET*/ )
			//donc, si le chemin qu'on connais est plus long que celui qu'on trouve, sachant que l'on est pas encore pass� par ici
		{
			setDistVers(dest, getDistVers(activeSom_) + var->getTemps()); //set le nouveau temps qui est plus petit
			setCheminVers(dest, getCheminVers(activeSom_) + " => " + to_string(dest->getCityNum() ) ); //set le nouveau chemin plus rapide

		}

	}
	//fin des updates
}

//fonction qui applique une it�ration de l'algorithme de Dijkstra, modifi� pour prendre en charge la charge des drones
/* TODO TODO TODO
"algorithme doit �viter de donner � un drone une chemin qui fait passer ses batteries
au-dessous de 20%. Si c�est le cas, on pr�f�rera une autre chemin plus co�teux en termes de temps
mais plus s�curitaire pour les citoyens. Si une telle trajectoire n�existe pas, c�est seulement � ce
moment-l� qu�on utilisera un drone 5.0 Amp�res. Si les m�mes conditions ne sont pas respect�es
non plus pour le drone 5.0 Amp�res, on refusera la livraison.e temps n�cessaire pour la recharge des batteries d�un drone
est de 20 minutes, quelque soit le type de drone ainsi que son niveau de d�charge. Le drone
s�arr�te pour recharger ses batteries d�s qu�il passe par une station de recharge, peu importe le
niveau de ses batteries. En quittant la station, ses batteries sont � 100% d��nergie."
*/
void Dijkstra::iterateUn() {

	//update du s et nbs
	s_[nbS_++] = activeSom_;

	//update des chemins et longueurs des voisins si n�c�ssaire
	updateVoisins();
	//et on regarde le plus court chemin depuis le chemin de d�part en m�me temps
	int indexDistMin = FindDistMin();

	//on le choisit et on change le sommet actif
	activeSom_ = g_->getSommets()[indexDistMin]; 
		

	//TODO


}

string Dijkstra::findChemin(int NumNodeDepart, int NumNodeArrivee) {
	activeSom_ = g_->getSommets()[NumNodeDepart - 1];
	bool success = true;
	string res = "";
	string leNod =  "( " + to_string(NumNodeArrivee) + " )";
	cout << PROMPT << leNod << endl;

	for (int i = 0; i < nbSommets_; i++)//d�finition du size des deux vecteurs
	{
		distChemins_[i] = INFINI;
		parcours_[i] = to_string(NULL);
	}

	//d�finition du point de d�part, on sous entend que 
	distChemins_[activeSom_->getCityNum() - 1] = 0;//set la distance � 0 pour le d�part
	parcours_[activeSom_->getCityNum() - 1] = to_string(activeSom_->getCityNum() )  ;//set le chemin parcouru � lui m�me pour le d�part

	while ((activeSom_ != g_->getSommets()[NumNodeArrivee - 1]) 
		&& success ) //drone utilis� n'est pas videt
		//tant que le noeud observ� n'est pas le noeud d'arriv�e ou lorsque les drones ne se rendent pas
	{	
		iterateUn();//une it�ration
	}

	if (success) {
		res = "[ " + parcours_[NumNodeArrivee - 1] + " ]  et le temps pris pour parcourir ce chemin sera " + to_string(distChemins_[NumNodeArrivee - 1]);
	}

	return res; 

}