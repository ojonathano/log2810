#pragma once
/********************************
* name: Algo.cpp
* date: 18 oct 2017
* desc: Fichier impl�mentant les algorithmes utilis�s dans le tp soit:
*		une variation de l'algorithme de Dijkstra
*		un algorithme pour cr�er un diagramme de Hasse
*********************************/

#include "../../Jonathan/TP1Log2810/Graphe.h"
#include "../DikjstraArcSommets/Arc.h"
#include "../DikjstraArcSommets/Sommet.h"
#include "../DikjstraArcSommets/IntelliNode.h"
//drones
//colis
#include <string>
#include <algorithm>
class Dijkstra
{
public:
	//constructeur par d�faut
	Dijkstra();
	//constructeur par param�tre
	Dijkstra(Graphe* g);
	//destructeur
	~Dijkstra();

	//getters
	int getDistVers(Sommet* s);
	string getCheminVers(Sommet*s);

	//setters
	void setActiveSommet(Sommet* a);
	void setGraph(Graphe* g);
	void setDistVers(Sommet* s, int p);
	void setCheminVers(Sommet*s, string c);

	//fonctions de traitement
	IntelliNode* FindNode(const Sommet* sommet);
	void updateVoisins();
	bool dansS(const Sommet* sommet);
	void iterateUn();
	string findChemin(int NumNodeDepart, int NumNodeArrivee);
	int FindDistMin(); 


private:
	//DRONES
	Graphe* g_;
	int nbSommets_;
	vector<IntelliNode>* Nodes_;
	Sommet* activeSom_;
	int* distChemins_;
	string* parcours_;
	Sommet** s_;
	int nbS_;
};

