#pragma once
/********************************
* name: Algo.cpp
* date: 18 oct 2017
* desc: Fichier impl�mentant les algorithmes utilis�s dans le tp soit:
*		une variation de l'algorithme de Dijkstra
*		un algorithme pour cr�er un diagramme de Hasse
*********************************/

//Graphe et inteliNode
#include "../../Jonathan/TP1Log2810/Graphe.h"
#include "../../Louis/DikjstraArcSommets/IntelliNode.h"
//drones
#include "../../Patrick/Drone3_3.h"
#include "../../Patrick/Drone5_0.h"
//colis
#include "../../Patrick/PoidsPlume.h"
#include "../../Patrick/PoidsMoyen.h"
#include "../../Patrick/PoidsLourd.h"

//librairies internes
#include <string>
#include <algorithm>

class Dijkstra
{
public:
	//constructeur par d�faut
	Dijkstra();
	//constructeur par param�tre
	Dijkstra(Graphe* g);
	//destructeur
	~Dijkstra();

	//seule m�thode utilisable de l'ext�rieur
	string findChemin(Colis* chargement ,int NumNodeDepart, int NumNodeArrivee);

private:
	//getters
	int getDistVers(Sommet* s);
	string getCheminVers(Sommet*s);
	int getDr3(Sommet* s);
	int getDr5(Sommet* s);
	IntelliNode* Dijkstra::getINactive();

	//setters
	void setActiveSommet(Sommet* a);
	void setGraph(Graphe* g);
	void setDistVers(Sommet* s, int p);
	void setCheminVers(Sommet*s, string c);
	void setDr3(Sommet* s, const int& perdu);
	void setDr5(Sommet* s, const int& perdu);

	//fonctions de traitement
	IntelliNode* FindNode(const Sommet* sommet);
	void updateVoisins();
	bool dansS(const Sommet* sommet);
	void iterateUn();
	int FindIndexDistMin(); 



	//DRONES
	Drone3_3 cheapD_;
	Drone5_0 luxuD_;
	//variables
	Graphe* g_;
	bool parcourable_;
	int nbSommets_;
	vector<IntelliNode>* nodes_;
	Sommet* activeSom_;
	int* distChemins_;
	int* autonomieDr3_;
	int* autonomieDr5_;
	string* parcours_;
	Sommet** s_;
	int nbS_;
};

