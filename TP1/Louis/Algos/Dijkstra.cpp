#include "Dijkstra.h"
const static string PROMPT = "Le chemin pour se rendre au sommet ";
#define INFINI -1

//constructeur par d�faut ===============================================================================================
Dijkstra::Dijkstra() : cheapD_(Drone3_3() ), luxuD_(Drone5_0() ), g_{ nullptr }, parcourable_{ false }, nbSommets_{NULL}
, activeSom_{ nullptr }, nodes_{ nullptr }, distChemins_{ nullptr }, autonomieDr3_{ nullptr }, autonomieDr5_{nullptr}
, parcours_{ nullptr }, s_{ nullptr }, nbS_{ NULL }
{}

//constructeur par param�tre ============================================================================================
Dijkstra::Dijkstra(Graphe* g):cheapD_(Drone3_3() ),luxuD_(Drone5_0() ), g_{g}, parcourable_{true},activeSom_{nullptr}, nbS_{NULL}
{
	nbSommets_ = g->getSommets().size();
	nodes_ = new vector<IntelliNode>;
	for (int i = 0; i < nbSommets_; i++){
		nodes_->push_back(IntelliNode(g->getSommets()[i], g));
	}//
	distChemins_ = new int[nbSommets_];
	autonomieDr3_ = new int[nbSommets_];
	autonomieDr5_ = new int[nbSommets_];
	parcours_ = new string[nbSommets_];
	s_ = new Sommet*[nbSommets_];
}

//destructeur ===========================================================================================================
Dijkstra::~Dijkstra()
{
	//on delete pas le g car par aggr�gation
	//delete du vecteur apr�s l'avoir vid�
	g_ = nullptr;
	parcourable_ = false;
	nodes_->clear();
	delete nodes_;
	nodes_ = nullptr;
	//on delete pas le activeSom, car par aggr�gation
	//delete des tableaux d'information
	delete[] distChemins_;
	distChemins_ = nullptr;
	delete[] autonomieDr3_;
	autonomieDr3_ = nullptr;
	delete[] autonomieDr5_;
	autonomieDr5_ = nullptr;
	delete[] parcours_;
	parcours_ = nullptr;
	delete[] s_;//juste le conteneur, les Sommets* �taient par aggr�gation
	s_ = nullptr;
	nbS_ = NULL;
}

//getters ===============================================================================================================
/*
Fonction qui renvoie la distance la plus courte vers un node donn�
au moment pr�sent des it�rations de l'algo de Dijkstra
�tant donn� que le tableau des sommets est tri� (car le fichier lui m�me l'est)
tous les tableaux qui en d�coulent sont tri�s aussi et donc acc�der � la case
#[num�ro de sommet]-1 acc�de � la distance
*/
int Dijkstra::getDistVers(Sommet* s) {
	return distChemins_[s->getCityNum() - 1];
}

//fonction qui renvoie le chemin le plus court connu au moment pr�sent des it�rations de l'algo de Dijkstra
string Dijkstra::getCheminVers(Sommet*s) {
	return parcours_[s->getCityNum() - 1];
}

int Dijkstra::getDr3(Sommet* s) {
	return autonomieDr3_[s->getCityNum() - 1];
}

int Dijkstra::getDr5(Sommet* s) {
	return autonomieDr5_[s->getCityNum() - 1];
}

IntelliNode* Dijkstra::getINactive() {
	return &(nodes_->at(activeSom_->getCityNum() - 1) );
}

//setters ==============================================================================================================
//set un nouveau sommet actif
void Dijkstra::setActiveSommet(Sommet* a) {
	activeSom_ = a;
}

//ajoute un graph � l'objet et toutes les informations qui en d�coulent
void Dijkstra::setGraph(Graphe* g) {
	g_ = g;
	parcourable_ = true;
	nbSommets_ = g->getSommets().size();
	for (int i = 0; i < nbSommets_; i++) {
		nodes_->push_back(IntelliNode(g->getSommets()[i], g));
	}
	distChemins_ = new int[nbSommets_];
	autonomieDr3_ = new int[nbSommets_];
	autonomieDr5_ = new int[nbSommets_];
	parcours_ = new string[nbSommets_];
	s_ = new Sommet*[nbSommets_];
	nbS_ = NULL;
}

//fonction qui assigne un nouveau poids � un parcours vers un Sommet depuis le point de d�part
void Dijkstra::setDistVers(Sommet* s, int d) {
	distChemins_[s->getCityNum() - 1] = d;
}

//fonction qui assigne un nouveau parcours vers un Sommet depuis le point de d�part
void Dijkstra::setCheminVers(Sommet*s, string c) {
	parcours_[s->getCityNum() - 1] = c;
}

void Dijkstra::setDr3(Sommet* s, const int& perdu) {
	cheapD_.setAutonomie(getDr3(s) );
	//mettre son autonomie � ce qu'il aurait en ayant parcouru le chemin jusqu'� pr�sent
	autonomieDr3_[s->getCityNum() - 1] = cheapD_.d�terminerPostDep(perdu,s->getCityRecharge() );
	//changer � l'autonomie calcul�e
	cheapD_.setAutonomie(100);
	//remettre � 100

}

void Dijkstra::setDr5(Sommet* s, const int& perdu) {
	luxuD_.setAutonomie(getDr5(s) );
	//mettre son autonomie � ce qu'il aurait en ayant parcouru le chemin jusqu'� pr�sent
	autonomieDr5_[s->getCityNum() - 1] = luxuD_.d�terminerPostDep(perdu, s->getCityRecharge());
	//changer � l'autonomie calcul�e
	luxuD_.setAutonomie(100);
	//remettre � 100

}

// fonctions de traitement ============================================================================================

//fonction qui renvoie un pointeur vers le Node voulu dans une collection d'IntelliNodes
IntelliNode* Dijkstra::FindNode(const Sommet* sommet) {
	return find_if(nodes_->begin(), nodes_->end(), [sommet](IntelliNode n) { return n.getNode() == sommet; })._Ptr;
}

//fonction qui d�termine si le Sommet donn� est dans le tableau s
bool Dijkstra::dansS(const Sommet* sommet) {
	for (int i = 0; i < nbS_; i++)
	{
		if (s_[i] == sommet)
			return true;
	}
	return false;
}

//fonction qui �value les longueurs des sommets pour trouver le plus court
int Dijkstra::FindIndexDistMin() {
	int min = INFINI;
	int indexMin = 0;
	for (int i = 0; i < nbSommets_; i++) {
		//verifier que cet index n'est pas dans S


		if ( (distChemins_[i] < min || min == INFINI) && (!dansS(g_->getSommets()[i]) ) && (distChemins_[i]!= INFINI) ) {
			min = distChemins_[i];
			indexMin = i;
		}
	}
	return indexMin;
}

void Dijkstra::updateVoisins() {
	//intellinode pour les voisins, on update les voisins
	for each (Arc* var in (getINactive()->getArcs() ) ) {
		//apr�s avoir trouv� le bon IntelliNode correspondant au sommet regard� dans le tableau, on regarde chacun de ses arcs.
		Sommet* dest = var->getS2();
		int NouvDistVersDest = getDistVers(activeSom_) + var->getTemps();
		if (var->getS2()->getCityRecharge())
			NouvDistVersDest += 20;//ajustement du temps selon s'il y a une borne de recharge ou pas

		if ( ( (getDistVers(dest) == INFINI) || ( NouvDistVersDest < getDistVers(dest) ) )/*fin du OU*/
			// (Infini < nouv chemin direct) ou chemin vers activeSom + le temps pour traverser l'arc var < le temps d�ja connu 
			&& (!dansS(dest) )/*fin du ET*/ )
			//donc, si le chemin qu'on connais est plus long que celui qu'on trouve, sachant que l'on est pas encore pass� par ici
		{
			setDistVers(dest, NouvDistVersDest ); //set le nouveau temps qui est plus petit
			setCheminVers(dest, getCheminVers(activeSom_) + " => " + to_string(dest->getCityNum() ) ); //set le nouveau chemin plus rapide
			//on set les autonomies des drones rendu ici
			setDr3(dest, NouvDistVersDest);
			setDr5(dest, NouvDistVersDest);

		}

	}
	//fin des updates
}

//fonction qui applique une it�ration de l'algorithme de Dijkstra, modifi� pour prendre en charge la charge des drones
/*
"algorithme doit �viter de donner � un drone une chemin qui fait passer ses batteries
au-dessous de 20%. Si c�est le cas, on pr�f�rera une autre chemin plus co�teux en termes de temps
mais plus s�curitaire pour les citoyens. Si une telle trajectoire n�existe pas, c�est seulement � ce
moment-l� qu�on utilisera un drone 5.0 Amp�res. Si les m�mes conditions ne sont pas respect�es
non plus pour le drone 5.0 Amp�res, on refusera la livraison. Le temps n�cessaire pour la recharge des batteries d�un drone
est de 20 minutes, quelque soit le type de drone ainsi que son niveau de d�charge. Le drone
s�arr�te pour recharger ses batteries d�s qu�il passe par une station de recharge, peu importe le
niveau de ses batteries. En quittant la station, ses batteries sont � 100% d��nergie."
*/
void Dijkstra::iterateUn() {

	//update du s et nbs
	s_[nbS_++] = activeSom_;

	//update des chemins et longueurs des voisins si n�c�ssaire
	updateVoisins();
	//et on regarde le plus court chemin depuis le chemin de d�part en m�me temps
	//d�termine le chemin � suivre
	int indexDistMin = FindIndexDistMin();



	//on le choisit, se rends jusqu'� lui et on change le sommet actif
	activeSom_ = g_->getSommets()[indexDistMin]; 
		


}

string Dijkstra::findChemin(Colis* chargement, int NumNodeDepart, int NumNodeArrivee) {
	//chargement des drones
	cheapD_.setChargement(chargement);
	luxuD_.setChargement(chargement);

	activeSom_ = g_->getSommets()[NumNodeDepart - 1];
	string res = "Impossible a parcourir avec nos drones, desole";
	string leNod =  "( " + to_string(NumNodeArrivee) + " ) est: ";
	cout << PROMPT << leNod << endl;

	for (int i = 0; i < nbSommets_; i++)//d�finition du size des deux vecteurs
	{
		distChemins_[i] = INFINI;
		parcours_[i] = to_string(NULL);
		autonomieDr3_[i] = 100;
		autonomieDr5_[i] = 100;
	}

	//d�finition du point de d�part, on sous entend que 
	distChemins_[activeSom_->getCityNum() - 1] = 0;//set la distance � 0 pour le d�part
	parcours_[activeSom_->getCityNum() - 1] = to_string(activeSom_->getCityNum() )  ;//set le chemin parcouru � lui m�me pour le d�part

	while ((activeSom_ != g_->getSommets()[NumNodeArrivee - 1]) ) //tant qu'on a pas trouv� le sommet voulu
		//tant que le noeud observ� n'est pas le noeud d'arriv�e ou lorsque les drones ne se rendent pas
	{	
		iterateUn();//une it�ration
	}
	cout << to_string(activeSom_->getCityNum()) << " : "<<endl 
		<<"energie restante en utilisant le drone 3.3 amperes: " <<	 to_string(getDr3(activeSom_)) << "%" << endl
		<<"energie restante en utilisant le drone 5.0 amperes: " 	<< to_string(getDr5(activeSom_)) <<"%"<< endl;

	if (parcourable_) {
		res = "[ " + parcours_[NumNodeArrivee - 1] + " ]  et le temps pris pour parcourir ce chemin sera de " + to_string(distChemins_[NumNodeArrivee - 1]) + " minutes.";
	}
	else {
	
	}

	parcourable_ = true;//si on se rends ici, on peux parcourir, mais peux-�tre que �a a �t� chang� par les m�thode 
	return res; 

}