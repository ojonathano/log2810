#include "pMoyen.h"
const static int POIDSTANDARD = 20;//% par 10 minutes


pMoyen::pMoyen()
{
	poids = POIDSTANDARD;
}

pMoyen::pMoyen(int p)
{
	poids = p;
}


pMoyen::~pMoyen()
{
}

int pMoyen::getPoids()
{
	return poids;
}

void pMoyen::setPoids(int p)
{
	poids = p;
}