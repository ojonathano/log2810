#pragma once
#include "colis.h"
class pLourd :
	public colis
{
public:
	pLourd();
	pLourd(int p);
	~pLourd();
	int getPoids();
	void setPoids(int p);

private:
	int poids;
};

