#include "pPlume.h"
const static int POIDSTANDARD = 10;//% par 10 minutes


pPlume::pPlume()
{
	poids = POIDSTANDARD;
}

pPlume::pPlume(int p)
{
	poids = p;
}

pPlume::~pPlume()
{
}

int pPlume::getPoids() 
{
	return poids;
}

void pPlume::setPoids(int p)
{
	poids = p;
}