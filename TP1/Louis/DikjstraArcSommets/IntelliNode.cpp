#include "IntelliNode.h"

/*Fonction qui cr�e un vecteur contenant tout les arcs partant de ce Sommet
	� partir de la liste de base d'arcs d'un graphe
*/
vector<Arc*> IntelliNode::makeSimpArc(Graphe* g){
	vector<Arc*> r;
	vector<Arc*> temp = g->getArcs();
	for (size_t i = 0; i < temp.size(); i++)
	{//tous les arcs qui partent de ou qui aboutissent sur ce Sommet
		if (temp[i]->getS1() == thisNode_) {
			Arc* tempo = new Arc(nullptr, temp[i]->getS2(), temp[i]->getTemps());
			r.push_back(tempo);
		}

		if (temp[i]->getS2() == thisNode_) {
			Arc* tempo = new Arc(nullptr, temp[i]->getS1(), temp[i]->getTemps());
			r.push_back(tempo);
		}
	}
	return r;
}

IntelliNode::IntelliNode(Sommet* tN, Graphe* g){
	thisNode_ = tN;
	arcs_ = makeSimpArc(g);
}

//constructeur par copie
IntelliNode::IntelliNode(IntelliNode const& toCopy) {
	thisNode_ = toCopy.thisNode_;
	vector<Arc*> r;
	for each (Arc* var in toCopy.arcs_)//cr�e des pointeurs vers une copie des arcs du premier IntelliNode
	{
		Arc* arcCopy = var;
		r.push_back(arcCopy);
	}

	arcs_ = r;
}



IntelliNode::~IntelliNode(){
	//composition par pointeurs donc...
	//delete de tous les pointeurs contenus dans arcs_
	arcs_.clear();
}

Sommet* IntelliNode::getNode() {
	return thisNode_;
}

int IntelliNode::getNum(){
	return thisNode_->getCityNum();
}

vector<Arc*> IntelliNode::getArcs(){
	return arcs_;
}

Arc* IntelliNode::getArcs(const int& s2) {
	return *find_if(arcs_.begin(), arcs_.end(), [s2](Arc* n) {return n->getS2()->getCityNum() == s2; });
}

int IntelliNode::getSize(){
	return arcs_.size();
}
