#include "Arc.h"
/********************************
* name: Arc.h
* date: 11 oct 2017
* desc: Classe impl�mentant les arcs d'un graphe
*		l'arc peux autant �tre trait� comme un arc orient� que non
*		orient�:		(sommet1)-=(temps)=>(sommet2)
*		non-orient�:	(sommet1)-=(temps)=-(sommet2)
*********************************/

//Constructeur
Arc::Arc() {
	sommet1_ = nullptr;
	sommet2_ = nullptr;
	temps_ = 0; 
} 

//constructeur par param�tre
Arc::Arc(Sommet* s1, Sommet* s2, int t) {
	sommet1_ = s1;
	sommet2_ = s2;
	temps_ = t;
}

//destructeur
Arc::~Arc() {
}

//setters
void Arc::setS1(Sommet* s1) {
	sommet1_ = s1;
}

void Arc::setS2(Sommet* s2) {
	sommet2_ = s2;
}

void Arc::setTemps(int t) {
	temps_ = t;
}

//getters
Sommet* Arc::getS1() {
	return sommet1_;
}

Sommet* Arc::getS2() {
	return sommet2_;
}

int Arc::getTemps() {
	return temps_;
}