#ifndef _ARC_H_
#define _ARC_H_

/********************************
* name: Arc.cpp
* date: 11 oct 2017
* desc: Classe impl�mentant les arcs d'un graphe
*		l'arc peux autant �tre trait� comme un arc orient� que non
*		orient�:		(sommet1)-=(temps)=>(sommet2)
*		non-orient�:	(sommet1)-=(temps)=-(sommet2)
*********************************/

#include <iostream>
#include "Sommet.h"

using namespace std; 

class Arc {

public: 
	//constructeurs-destructeurs
	Arc();
	Arc(Sommet* s1, Sommet* s2, int t);
	~Arc();
	//setters
	void setS1(Sommet* s1);
	void setS2(Sommet* s2);
	void setTemps(int t);
	//getters
	Sommet* getS1();
	Sommet* getS2();
	int getTemps();

private:
	Sommet* sommet1_;
	Sommet* sommet2_;
	int temps_;
};

#endif