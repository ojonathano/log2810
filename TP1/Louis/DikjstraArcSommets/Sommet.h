/********************************
* name: Sommet.h
* date: 11 oct 2017
* desc: classe pour  implémenter des sommets dans des graphes
*********************************/


#ifndef _SOMMET_H_
#define _SOMMET_H_

const int INITIAL_SIZE = 10; 

#include <iostream>

using namespace std; 

class Sommet {
public: 
	//constructeurs-destructeurs
	Sommet();
	Sommet(int& num, bool& rech);
	Sommet(int& num, int& rech);
	~Sommet();
	//getters
	int getCityNum(); 
	bool getCityRecharge();
	//setters
	void setCityNum(const int& num);
	void setCityRecharge(const bool& rech);
	void setCityRecharge(const int& rech);

private:
	int cityNum_; 
	bool cityRecharge_; 
};

#endif; 