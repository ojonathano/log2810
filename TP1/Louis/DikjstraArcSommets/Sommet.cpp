/********************************
* name: Sommet.cpp
* date: 11 oct 2017
* desc: classe pour  impl�menter des sommets dans des graphes
*********************************/

#include "Sommet.h"

//constucteur
Sommet::Sommet() {
	cityNum_ = NULL; 
	cityRecharge_ = NULL; 
}

//constructeur par param�tre
Sommet::Sommet(int& num,bool& rech) {
	cityNum_ = num;
	cityRecharge_ = rech;
}

//constructeur par param�tre
Sommet::Sommet(int& num, int& rech) {
	cityNum_ = num;
	cityRecharge_ = (rech==1);
}


//destructeur
Sommet::~Sommet() {
}

//setters
void Sommet::setCityNum(const int& num) {
	cityNum_ = num; 
}

void Sommet::setCityRecharge(const bool& rech) {
	cityRecharge_ = rech; 
}

void Sommet::setCityRecharge(const int& rech) {
	cityRecharge_ = (rech==1);
}

//getters
int Sommet::getCityNum() {
	return cityNum_; 
}

bool Sommet::getCityRecharge() {
	return cityRecharge_;
}

