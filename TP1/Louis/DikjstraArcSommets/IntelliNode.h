#pragma once
/********************************
* name: IntelliNode.cpp
* date: 18 oct 2017
* desc: Fichier implémentant les Nodes utilisés dans l'algorithme de Dijkstra 
*********************************/

#include "Arc.h"
#include "Sommet.h"
#include "../../Jonathan/TP1Log2810/Graphe.h"
#include <vector>
#include <algorithm>
class IntelliNode
{
public:
	IntelliNode(Sommet* tN, Graphe* g);
	IntelliNode(IntelliNode const& toCopy);
	~IntelliNode();
	Sommet* getNode();
	int getNum();
	vector<Arc*> getArcs();
	Arc* getArcs(const int& s2);
	int getSize();

private:
	vector<Arc*> makeSimpArc(Graphe* g);

	Sommet* thisNode_;//le sommet d'ou on part
	vector<Arc*> arcs_;//tous les arcs partant du Node
};

